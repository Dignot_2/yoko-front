FROM nginx

WORKDIR client

RUN curl -fsSL https://deb.nodesource.com/setup_17.x | bash -
RUN apt-get install -y nodejs

COPY package*.json ./

RUN npm install

COPY . .

COPY nginx.conf /etc/nginx/conf.d/default.conf

RUN npm run build:prod

RUN rm -r /usr/share/nginx/html/*

RUN cp -a dist/. /usr/share/nginx/html
