import { useState, useEffect } from "react";
import { useAppSelector } from "providers";
import TransferModal, { ITransferModalProps } from "entities/transferModal";
import { depositApi } from "../../model/api/depositApi";
import { getUserBalanceSelector } from "../../model/selectors/getUserBalanceSelector";
interface IUserDepositModalProps {
  onClose: () => void;
  isOpen: boolean;
}

const UserDepositModal = (props: IUserDepositModalProps) => {
  const { onClose, isOpen } = props;
  const balance = useAppSelector(getUserBalanceSelector)!;

  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState<string | null>(null);
  const [userLink, setUserLink] = useState<string | null>(null);

  const handeUserDeposit = (deposit: number) => {
    setIsLoading(true);
    depositApi({ deposit })
      .then(({ link }) => setUserLink(link))
      .catch((error) => setError(error))
      .finally(() => setIsLoading(false));
  };

  useEffect(() => {
    if (userLink) {
      const link = document.createElement("a");
      link.href = userLink;
      link.target = "_blank";
      link.click();
      onClose();
    }
  }, [userLink]);

  const transferModalData: ITransferModalProps = {
    ...{ isOpen },
    handleCLose: () => onClose(),
    mainError: error,
    isLoading,
    type: "add",
    title: "actions.card.user.main.balance.modal.title",
    action: {
      title: "actions.card.user.main.balance.button.deposit",
      action: handeUserDeposit,
    },
    from: {
      label: "actions.card.user.main.balance.modal.from.label",
      min: 10,
      placeholder: "actions.card.user.main.balance.modal.from.placeholder",
    },
    to: {
      label: "actions.card.user.main.balance.modal.to.label",
      initValue: balance,
    },
  };

  return <TransferModal {...transferModalData} />;
};

export default UserDepositModal;
