import { IStore } from "providers";

export const getUserBalanceSelector = (state: IStore) => state.userReduser.user.balance;
