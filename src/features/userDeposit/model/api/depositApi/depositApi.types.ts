export type IDepositApi = {
  deposit: number;
};

export type IDepositResponse = {
  link: string;
};
