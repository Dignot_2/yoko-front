import { IStore } from "providers";

export const getUserReferralBalanceSelector = (state: IStore) =>
  state.userReduser.user.referralBalance;
