import { createAsyncThunk } from "@reduxjs/toolkit";
import type { AsyncThunkConfig } from "src/providers/store";
import { ITransferReferralBalanceDto } from "../../api/transferReferralBalanceApi";

export const transferReferralBalanceAction = createAsyncThunk<
  ITransferReferralBalanceDto,
  ITransferReferralBalanceDto,
  AsyncThunkConfig
>("user/transfer/referral/balance", async (transferReferralBalanceDto, { rejectWithValue }) => {
  try {
    return transferReferralBalanceDto;
  } catch (e: any) {
    return rejectWithValue(e);
  }
});
