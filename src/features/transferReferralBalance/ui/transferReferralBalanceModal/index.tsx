import { useState } from "react";
import { useTranslation } from "react-i18next";
import { useAppDispatch, useAppSelector } from "providers";
import TransferModal, { ITransferModalProps } from "entities/transferModal";
import { transferReferralBalanceApi } from "../../model/api/transferReferralBalanceApi";
import { transferReferralBalanceAction } from "../../model/actions/transferReferralBalanceAction";
import { getUserReferralBalanceSelector } from "../../model/selectors/getUserReferralBalanceSelector";

interface ITransferReferralBalanceModalProps {
  onClose: () => void;
  isOpen: boolean;
}

const TransferReferralBalanceModal = (props: ITransferReferralBalanceModalProps) => {
  const { onClose, isOpen } = props;
  const { t } = useTranslation();
  const dispatch = useAppDispatch();

  const referralBalance = useAppSelector(getUserReferralBalanceSelector)!;
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState<string | null>(null);

  const handleTransferDeposit = (deposit: number) => {
    transferReferralBalanceApi({ deposit })
      .then(() => dispatch(transferReferralBalanceAction({ deposit })))
      .catch((error) => setError(error || "Server error"))
      .finally(() => {
        setIsLoading(false);
        onClose();
      });
  };

  const transferModalData: ITransferModalProps = {
    ...{ isOpen },
    handleCLose: () => onClose(),
    mainError: error,
    isLoading,
    type: "remove",
    title: "referral.card.modal.title",
    action: {
      title: "referral.card.modal.button.title",
      action: handleTransferDeposit,
    },
    from: {
      label: "referral.card.modal.from.label",
      initValue: 0,
      max: referralBalance,
      placeholder: "referral.card.modal.from.placeholder",
    },
    to: {
      label: "referral.card.modal.to.label",
      initValue: referralBalance,
    },
  };

  return <TransferModal {...transferModalData} />;
};

export default TransferReferralBalanceModal;
