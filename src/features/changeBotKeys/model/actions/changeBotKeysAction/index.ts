import { createAsyncThunk } from "@reduxjs/toolkit";
import type { AsyncThunkConfig } from "providers";
import { changeBotKeysApi } from "../../";
import type { IChangeBotKeysData } from "../../../";

export const changeBotKeysAction = createAsyncThunk<
  IChangeBotKeysData,
  IChangeBotKeysData,
  AsyncThunkConfig
>("bot/change/keys", async (changeBotKeysDto, { rejectWithValue }) => {
  try {
    return await changeBotKeysApi(changeBotKeysDto);
  } catch (e: any) {
    return rejectWithValue(e);
  }
});
