import { IStore } from "providers";

export const changeKeysIsLoadingSelector = (state: IStore) => state.botReducer.changeBotKeysLoading;
