import { IStore } from "providers";

export const botKeysSelectors = (state: IStore) => state.botReducer.keys;
