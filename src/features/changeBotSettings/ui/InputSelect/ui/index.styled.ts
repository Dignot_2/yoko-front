import styled from "@emotion/styled";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";

export const InputSelectWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 40px;
  gap: 10px;
  box-sizing: border-box;
  position: relative;
`;

export const InputSelectLabel = styled.label`
  font-style: normal;
  font-weight: 400;
  font-size: 15px;
  line-height: 20px;
  letter-spacing: -0.02em;
  color: #8f9bba;
  width: 136px;
  max-width: 136px;
  text-align: left;
`;

export const SelectBlock = styled.div`
  padding: 13px 20px;
  flex: 3 1 auto;
  height: 40px;
  border: none;
  background: #f4f7fe;
  border-radius: 49px;
  width: 186px;
  min-width: 186px;
  max-width: 186px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  overflow: hidden;
`;

export const SelectBlockText = styled.h1`
  text-overflow: ellipsis;
  line-clamp: 1;
  white-space: nowrap;
  overflow: hidden;
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 16px;
  letter-spacing: 0.3px;
  color: #5f5cec;
  max-width: 135px;
`;

export const SelectBlockIcon = styled.div``;

type IMenuWrapperProps = {
  leftPositions: string;
};

export const MenuWrapper = styled.div<IMenuWrapperProps>`
  position: absolute;
  width: 186px;
  height: 172px;
  top: 45px;
  left: ${({ leftPositions }) => leftPositions};
  padding: 17px 0px 17px 0px;
  border-radius: 30px 30px 20px 20px;
  border: 2px;
  gap: 24px;
  background: #f5f7fd;
  border: 2px solid #f5f7fd;
  box-shadow: 0px 25px 20px -25px #14198f26;
  overflow: hidden;
  overflow-y: auto;
  z-index: 111;
`;

export const MenuBlock = styled(Menu)(() => ({
  ".MuiMenu-paper": {
    width: "186px",
    height: "214px",
    background: "#F5F7FD",
    border: "2px solid #F5F7FD",
    boxShadow: " 0px 25px 20px -25px rgba(20, 25, 143, 0.15)",
    gap: "8px",
  },
}));

export const MenuItemBlock = styled(MenuItem)(() => ({
  justifyContent: "space-between",
  "& > p": {
    fontStyle: "normal",
    fontWeight: 500,
    fontSize: "16px",
    lineHeight: "16px",
    letterSpacing: "0.3px",
    color: "#5F5CEC",
    height: "26px",
  },
}));

export const MenuItemIconBlock = styled.div`
  width: 16px;
  height: 16px;
`;
