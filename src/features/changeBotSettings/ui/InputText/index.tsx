import { useEffect, useRef, ChangeEvent, useState } from "react";
import * as Styled from "./index.styled";

type InputTextFieldProps = {
  handleChange: (value: string) => void;
};

const InputText = (props: InputTextFieldProps) => {
  const { handleChange } = props;

  const inputRef = useRef();

  const [value, setValue] = useState("");

  useEffect(() => {
    if (inputRef.current) {
      const input = inputRef.current as HTMLInputElement;
      input.focus();
    }
  }, []);

  const onChange = (evt: ChangeEvent<HTMLInputElement>) => {
    const text = evt.target.value.toUpperCase();
    handleChange(text);
    setValue(text);
  };

  return <Styled.InputText type="text" ref={inputRef} {...{ onChange, value }} />;
};

export default InputText;
