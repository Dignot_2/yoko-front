import styled from "@emotion/styled";

export const InputTextWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 40px;
  gap: 10px;
  box-sizing: border-box;
`;

export const InputTextLabel = styled.label`
  font-style: normal;
  font-weight: 400;
  font-size: 15px;
  line-height: 20px;
  letter-spacing: -0.02em;
  color: #8f9bba;
  width: 136px;
  max-width: 136px;
  text-align: left;
`;

export const InputText = styled.input`
  padding: 10px 20px;

  width: 100%;
  height: 40px;
  border: none;
  background: #f4f7fe;
  border-radius: 49px;
  color: #5f5cec;
  font-style: "normal";
  font-weight: 500;
  font-size: "16px";
  line-height: "16px";
  letter-spacing: "0.3px";

  &:focus {
    border: none !important;
    outline: 0;
    outline-offset: 0;
  }
`;
