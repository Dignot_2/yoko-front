import styled from "@emotion/styled";

export const InputNumberWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 40px;
  gap: 10px;
  box-sizing: border-box;
`;

export const InputNumberLabel = styled.label`
  font-style: normal;
  font-weight: 400;
  font-size: 15px;
  line-height: 20px;
  letter-spacing: -0.02em;
  color: #8f9bba;
  width: 136px;
  max-width: 136px;
  text-align: left;
`;

export const InputNumber = styled.input`
  padding: 10px 20px;

  flex: 3 1 auto;
  height: 40px;
  border: none;
  background: #f4f7fe;
  border-radius: 49px;
  color: #5f5cec;
  width: 186px;
  min-width: 186px;
  max-width: 186px;

  &:focus {
    border: none !important;
    outline: 0;
    outline-offset: 0;
  }

  &::-webkit-outer-spin-button,
  &::-webkit-inner-spin-button {
    -webkit-appearance: none;
  }
`;
