import { useFormikContext } from "formik";
import get from "lodash/get";
import * as Styled from "./index.styled";

type InputNumberFieldProps = {
  name: string;
  label: string;
};

const InputNumber = (props: InputNumberFieldProps) => {
  const { name, label } = props;
  const { values, setFieldValue } = useFormikContext();
  const value = get(values, name, "");
  const handleChangeFields = (value: string) => {
    setFieldValue(name, +value);
  };
  return (
    <Styled.InputNumberWrapper>
      <Styled.InputNumberLabel htmlFor={name}>{label}</Styled.InputNumberLabel>
      <Styled.InputNumber
        id={name}
        type="number"
        step="0.1"
        {...{ name, value }}
        onChange={(evt) => handleChangeFields(evt.target.value)}
      />
    </Styled.InputNumberWrapper>
  );
};

export default InputNumber;
