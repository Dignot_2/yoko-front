import styled from "@emotion/styled";

export const BooleanSelectWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 40px;
  box-sizing: border-box;
  gap: 10px;
`;
export const BooleanSelectLabel = styled.label`
  font-style: normal;
  font-weight: 400;
  font-size: 15px;
  line-height: 20px;
  letter-spacing: -0.02em;
  color: #8f9bba;
  width: 136px;
  max-width: 136px;
  text-align: left;
`;

export const BooleanInputBlock = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-around;
  box-sizing: border-box;
  border: none;
  background: #f4f7fe;
  border-radius: 49px;
  color: #5f5cec;
  width: 186px;
  min-width: 186px;
  max-width: 186px;
  height: 40px;
`;

export const BooleanActionsBlock = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-flow: column nowrap;
`;

export const BooleanInput = styled.input`
  border: none !important;
  width: 30%;
  height: 100%;
  outline: 0;
  background: none;
  outline-offset: 0;
  &:focus {
    border: none !important;
    outline: 0;
    outline-offset: 0;
  }
`;
