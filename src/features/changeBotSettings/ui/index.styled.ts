import styled from "@emotion/styled";

export const ChangeBotSettingsWrapper = styled.div`
  margin-top: 40px;
  width: 100%;
  display: flex;
  display: flex;
  flex-flow: column;
  align-items: center;
  justify-content: flex-start;
  gap: 40px;
`;

export const ChangeBotSettingsBlock = styled.div`
  padding: 24px 52px 48px;
  width: 100%;
  background: #ffffff;
  border: 2px solid #f5f7fd;
  border-radius: 20px;
  display: flex;
  flex-flow: column;
  align-items: center;
  justify-content: flex-start;
  gap: 24px;

  @media screen and (max-width: 670px) {
    height: fit-content;
    padding: 10px 10px;
  }
`;

export const TextFieldWrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: flex-start;

  @media screen and (max-width: 670px) {
    flex-flow: column;
    gap: 24px;
  }
`;

export const TextFieldBlock = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-flow: column;
  align-items: center;
  justify-content: flex-start;
  gap: 24px;
`;

export const TextFieldTitle = styled.h1`
  font-style: normal;
  font-weight: 600;
  font-size: 20px;
  line-height: 24px;
  text-align: center;
  letter-spacing: 0.3px;
  color: #181938;

  @media screen and (max-width: 670px) {
    color: #2b3674;
  }
`;
