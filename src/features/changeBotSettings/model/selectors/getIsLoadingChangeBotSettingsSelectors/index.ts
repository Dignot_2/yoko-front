import { IStore } from "providers";

export const getIsLoadingChangeBotSettingsSelectors = (state: IStore) =>
  state.botReducer.changeBotSettingsLoading;
