import { useState } from "react";
import IconButton from "@mui/material/IconButton";
import LogoutIcon from "@mui/icons-material/Logout";
import { useAppDispatch } from "providers";
import UserLogOutModal from "./UserLogOutModal";
const userLogout = () => {
  const dispatch = useAppDispatch();

  const [isOpen, setIsOpen] = useState(false);

  const handleOpenModal = () => {
    setIsOpen(true);
  };

  const onClose = () => {
    setIsOpen(false);
  };

  return (
    <>
      <UserLogOutModal {...{ isOpen, onClose }} />
      <IconButton onClick={() => handleOpenModal()}>
        <LogoutIcon />
      </IconButton>
    </>
  );
};

export default userLogout;
