export type ILogoutBlockProps = {
  isOpen: boolean;
  onClose: () => void;
};
