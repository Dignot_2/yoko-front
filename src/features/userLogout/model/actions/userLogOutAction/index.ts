import { createAction } from "@reduxjs/toolkit";

export const userLogOutAction = createAction("user/userLogOutAction");
