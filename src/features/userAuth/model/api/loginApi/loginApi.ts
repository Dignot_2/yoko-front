import http from "shared/api/axios";
import type { ILoginDto } from "./loginApi.types";

export const loginApi = (loginDTO: ILoginDto) => http.post<null, ILoginDto>("user/login", loginDTO);
