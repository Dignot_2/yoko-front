export type ILoginDto = {
  login: string;
  password: string;
};
