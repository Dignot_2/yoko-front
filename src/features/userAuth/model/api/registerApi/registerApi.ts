import http from "shared/api/axios";
import type { IRegisterDto } from "./registerApi.types";

export const registerApi = (registerDto: IRegisterDto) =>
  http.post<null, IRegisterDto>("user/register", registerDto);
