export type IRegisterDto = {
  name: string;
  email: string;
  password: string;
  passwordRetry: string;
  referralCode: string;
};
