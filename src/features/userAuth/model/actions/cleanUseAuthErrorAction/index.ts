import { createAction } from "@reduxjs/toolkit";

export const cleanUseAuthErrorAction = createAction("user/cleanAuthError");
