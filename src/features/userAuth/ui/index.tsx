import { useState } from "react";
import AuthModal from "./authModal";
import RegisterModal from "./registerModal";
import type { IUserAuthProps } from "./index.types";

const UserAuth = (props: IUserAuthProps) => {
  const { type, renderComponent } = props;
  const [authType, setAuthType] = useState<string | null>(null);

  const handleClick = () => {
    setAuthType(type);
  };

  const onClose = () => setAuthType(null);

  const handleChoiseAuthType = () => {
    if (authType === "auth") setAuthType("register");
    if (authType === "register") setAuthType("auth");
  };
  return (
    <>
      {authType === "auth" && <AuthModal {...{ onClose, handleChoiseAuthType }} />}
      {renderComponent({ handleClick })}
      {authType === "register" && <RegisterModal {...{ onClose, handleChoiseAuthType }} />}
    </>
  );
};

export default UserAuth;
