export type IRenderComponentProps = {
  handleClick: () => void;
};

export type IUserAuthProps = {
  type: "auth" | "register";
  renderComponent: (props: IRenderComponentProps) => JSX.Element;
};
