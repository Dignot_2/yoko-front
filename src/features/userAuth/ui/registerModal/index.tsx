import { useState } from "react";
import { useTranslation } from "react-i18next";
import { Formik } from "formik";
import ButtonLib from "@mui/material/Button";
import { useAppDispatch } from "providers";
import ModalPage from "shared/ui/ModalPage/ModalPage";
import Button from "shared/ui/Button";
import InputField from "shared/ui/InputField";
import SuccessIconBlock from "shared/ui/SuccessIconBlock";
import { registerApi } from "../../model/api/registerApi";
import { initialValues, validationSchema, RegisterInitialValues } from "./index.shema";
import * as Styled from "./index.styled";

type IRegisterModalProps = {
  onClose: () => void;
  handleChoiseAuthType: () => void;
};

const RegisterModal = (props: IRegisterModalProps) => {
  const { t } = useTranslation();
  const dispatch = useAppDispatch();

  const { onClose, handleChoiseAuthType } = props;

  const [isLoading, setIsLoading] = useState(false);
  const [successRegister, setSuccessRegister] = useState(false);
  const [error, setError] = useState<string | null>(null);

  const handleSubmit = (values: RegisterInitialValues) => {
    setIsLoading(true);
    registerApi(values)
      .then(() => setSuccessRegister(true))
      .catch((e) => setError(e || "Server Error"))
      .finally(() => setIsLoading(false));
  };

  const handleChangeRigisterMode = () => {
    handleChoiseAuthType();
  };

  return (
    <ModalPage
      onClose={() => {
        setError(null);
        setSuccessRegister(false);
        onClose();
      }}
    >
      <Styled.RegisterWrapper>
        {!successRegister && (
          <>
            <Styled.RegisterTitle>{t("register.title")}</Styled.RegisterTitle>
            <Styled.RegisterInputWrapper>
              <Formik
                onSubmit={handleSubmit}
                validateOnChange={false}
                {...{ initialValues, validationSchema }}
              >
                {({ handleChange, errors, handleSubmit }) => (
                  <>
                    <Styled.RegisterInputWrapper>
                      <InputField
                        type="text"
                        name="name"
                        placeholder={t("register.input.name")}
                        handleChange={handleChange}
                      />
                      <InputField
                        type="text"
                        name="email"
                        placeholder={t("register.input.email")}
                        handleChange={handleChange}
                      />
                      <InputField
                        type="password"
                        name="password"
                        placeholder={t("register.input.password")}
                        handleChange={handleChange}
                      />
                      <InputField
                        type="password"
                        name="passwordRetry"
                        placeholder={t("register.input.password.retry")}
                        handleChange={handleChange}
                      />
                      <InputField
                        type="text"
                        name="referralCode"
                        placeholder={t("register.input.referral.code")}
                        handleChange={handleChange}
                      />
                    </Styled.RegisterInputWrapper>
                    <Styled.RegisterInformationBlock>
                      <Styled.RegisterError>
                        {(errors.name ||
                          errors.password ||
                          errors.passwordRetry ||
                          errors.email ||
                          errors.referralCode ||
                          error) &&
                          t(
                            errors.name ||
                              errors.password ||
                              errors.passwordRetry ||
                              errors.email ||
                              errors.referralCode ||
                              error
                          )}
                      </Styled.RegisterError>
                      <Styled.RegisterChangeModeForRegister>
                        {t("register.button.auth.message")}
                        <ButtonLib
                          sx={{ color: "#5F5CEC; text-transform: none;" }}
                          variant="text"
                          size="small"
                          onClick={() => handleChangeRigisterMode()}
                        >
                          {t("register.button.auth")}
                        </ButtonLib>
                      </Styled.RegisterChangeModeForRegister>
                      <Button
                        size="medium"
                        color="primary"
                        onClick={() => handleSubmit()}
                        {...{ isLoading }}
                      >
                        {t("register.button.submit")}
                      </Button>
                    </Styled.RegisterInformationBlock>
                  </>
                )}
              </Formik>
            </Styled.RegisterInputWrapper>
          </>
        )}
        {successRegister && (
          <Styled.RegisterSuccsessWrapper>
            <SuccessIconBlock />
            <Styled.RegisterSuccsessTitle>
              {t("register.information.title")}
            </Styled.RegisterSuccsessTitle>
            <Styled.RegisterSuccsessDescription>
              {t("register.information.description")}
            </Styled.RegisterSuccsessDescription>
          </Styled.RegisterSuccsessWrapper>
        )}
      </Styled.RegisterWrapper>
    </ModalPage>
  );
};

export default RegisterModal;
