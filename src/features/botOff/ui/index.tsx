import { useTranslation } from "react-i18next";
import ButtonLib from "@mui/material/Button";
import { useAppDispatch, useAppSelector } from "providers";
import { botStatusSelector, changeBotStatusActions } from "features/botOn";
import { BotStatus } from "shared/constants/botStatus";

const BotOf = () => {
  const { t } = useTranslation();
  const dispatch = useAppDispatch();

  const status = useAppSelector(botStatusSelector);

  const handleBotOff = () => {
    dispatch(changeBotStatusActions({ status: BotStatus.OFF }));
  };

  const disabled =
    status === BotStatus.OFF || status === BotStatus.CREATE || status === BotStatus.ERROR;

  return (
    <ButtonLib
      sx={{
        color:
          "#181938; height: 45px; text-transform: none; font-style: normal; font-weight: 500; font-size: 14px; line-height: 12px; letter-spacing: 0.3px;",
      }}
      fullWidth
      variant="text"
      size="small"
      onClick={() => handleBotOff()}
      {...{ disabled }}
    >
      <p>{t("bot.card.button.off")}</p>
    </ButtonLib>
  );
};

export default BotOf;
