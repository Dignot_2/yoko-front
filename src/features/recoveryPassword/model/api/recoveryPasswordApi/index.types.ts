export type IRecoveryPasswordApi = {
  recoveryCode: string;
  password: string;
};
