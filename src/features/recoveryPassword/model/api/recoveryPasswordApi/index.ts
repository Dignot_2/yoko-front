import http from "shared/api/axios";
import type { IRecoveryPasswordApi } from "./index.types";

export const recoveryPasswordApi = (recoveryPasswordDto: IRecoveryPasswordApi) =>
  http.post<null, IRecoveryPasswordApi>("user/password/recovery", recoveryPasswordDto);
