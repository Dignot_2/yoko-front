import styled from "@emotion/styled";

export const RecoveryPasswordBlock = styled.div`
  padding: 0 100px;
  display: flex;
  flex-flow: column;
  align-items: center;
  justify-content: flex-start;
  width: 100%;

  @media screen and (max-width: 670px) {
    padding: 0 16px;
  }
`;

export const RecoveryPasswordInformation = styled.div`
  display: flex;
  flex-flow: column;
  align-items: center;
  justify-content: flex-start;
  gap: 24px;
`;

export const RecoveryPasswordTitle = styled.h3`
  font-style: normal;
  font-weight: 600;
  font-size: 40px;
  line-height: 48px;
  text-align: center;
  color: #181938;

  @media screen and (max-width: 670px) {
    font-size: 30px;
    line-height: 32px;
  }
`;

export const RecoveryPasswordDescription = styled.h3`
  font-style: normal;
  font-weight: 400;
  font-size: 16px;
  line-height: 24px;
  text-align: center;
  letter-spacing: 0.3px;
  color: #181938;
`;

export const RecoveryPasswordInputBlock = styled.div`
  margin-top: 40px;
  display: flex;
  flex-flow: column;
  align-items: center;
  justify-content: flex-start;
  gap: 10px;
`;

export const RecoveryPasswordActionsBlock = styled.div`
  margin-top: 25%;
  display: flex;
  flex-flow: column;
  align-items: center;
  justify-content: center;
  width: 100%;
  gap: 20px;
`;

export const RecoveryPasswordSuccsessWrapper = styled.div`
  display: flex;
  flex-flow: column;
  align-items: center;
  justify-content: center;
  gap: 24px;
`;

export const RecoveryPasswordSuccsessTitle = styled.h1`
  font-style: normal;
  font-weight: 600;
  font-size: 40px;
  line-height: 48px;
  text-align: center;
  color: #181938;

  @media screen and (max-width: 670px) {
    font-size: 30px;
    line-height: 32px;
  }
`;

export const RecoveryPasswordError = styled.p`
  font-style: normal;
  font-weight: 400;
  font-size: 10px;
  line-height: 12px;
  color: #f87c92;
`;

export const RecoveryPasswordUserError = styled.p`
  font-size: 16px;
  font-weight: 400;
  line-height: 24px;
  letter-spacing: 0.30000001192092896px;
  text-align: center;
  color: #181938;
`;
