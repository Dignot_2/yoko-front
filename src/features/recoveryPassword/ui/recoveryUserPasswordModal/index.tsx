import { useState } from "react";
import { useTranslation } from "react-i18next";
import { Formik } from "formik";
import ModalPage from "shared/ui/ModalPage/ModalPage";
import InputField from "shared/ui/InputField";
import Button from "shared/ui/Button";
import SuccessIconBlock from "shared/ui/SuccessIconBlock";
import ErrorIconBlock from "shared/ui/ErrorIconBlock";
import { recoveryPasswordApi } from "../../model/api/recoveryPasswordApi";
import { IRecoveryPasswordInitialValues, initialValues, validationSchema } from "./index.shema";
import * as Styled from "./index.styled";

interface IRecoveryUserPasswordModalProps {
  recoveryCode: string;
}

const RecoveryUserPasswordModal = (props: IRecoveryUserPasswordModalProps) => {
  const { recoveryCode } = props;
  const { t } = useTranslation();

  const [successRecoveryPassword, setSuccessRecoveryPassword] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [open, setOpen] = useState(!!recoveryCode);
  const [error, setError] = useState<string | null>(null);

  const handleSubmit = ({ password }: IRecoveryPasswordInitialValues) => {
    setIsLoading(true);
    recoveryPasswordApi({ password, recoveryCode })
      .then(() => setSuccessRecoveryPassword(true))
      .catch((error) => setError(error || "Server error"))
      .finally(() => setIsLoading(true));
  };

  const onClose = () => {
    setOpen(false);
  };

  if (open) {
    return (
      <ModalPage {...{ onClose }}>
        <Styled.RecoveryPasswordBlock>
          {!successRecoveryPassword && !error && (
            <Formik
              onSubmit={handleSubmit}
              validateOnChange={false}
              {...{ initialValues, validationSchema }}
            >
              {({ handleChange, handleSubmit, errors }) => (
                <>
                  <Styled.RecoveryPasswordInformation>
                    <Styled.RecoveryPasswordTitle>
                      {t("recovery.title")}
                    </Styled.RecoveryPasswordTitle>
                    <Styled.RecoveryPasswordDescription>
                      {t("recovery.description")}
                    </Styled.RecoveryPasswordDescription>
                  </Styled.RecoveryPasswordInformation>
                  <Styled.RecoveryPasswordInputBlock>
                    <InputField
                      type="password"
                      name="password"
                      placeholder={t("recovery.input.password")}
                      handleChange={handleChange}
                    />
                    <InputField
                      type="password"
                      name="passwordRepeat"
                      placeholder={t("recovery.input.retry.password")}
                      handleChange={handleChange}
                    />
                    <Styled.RecoveryPasswordActionsBlock>
                      {errors.passwordRepeat && (
                        <Styled.RecoveryPasswordError>
                          {t(errors.passwordRepeat)}
                        </Styled.RecoveryPasswordError>
                      )}
                      <Button
                        size="medium"
                        color="primary"
                        isLoading={isLoading}
                        onClick={() => handleSubmit()}
                      >
                        {t("recovery.submit")}
                      </Button>
                    </Styled.RecoveryPasswordActionsBlock>
                  </Styled.RecoveryPasswordInputBlock>
                </>
              )}
            </Formik>
          )}
          {successRecoveryPassword && (
            <Styled.RecoveryPasswordSuccsessWrapper>
              <SuccessIconBlock />
              <Styled.RecoveryPasswordSuccsessTitle>
                {t("recovery.information.success.title")}
              </Styled.RecoveryPasswordSuccsessTitle>
            </Styled.RecoveryPasswordSuccsessWrapper>
          )}
          {error && (
            <Styled.RecoveryPasswordSuccsessWrapper>
              <ErrorIconBlock />
              <Styled.RecoveryPasswordSuccsessTitle>
                {t("table.deposit.status.error")}
              </Styled.RecoveryPasswordSuccsessTitle>
              <Styled.RecoveryPasswordUserError>{error}</Styled.RecoveryPasswordUserError>
            </Styled.RecoveryPasswordSuccsessWrapper>
          )}
        </Styled.RecoveryPasswordBlock>
      </ModalPage>
    );
  }

  return null;
};

export default RecoveryUserPasswordModal;
