import * as Yup from "yup";

export type IRecoveryPasswordInitialValues = {
  password: string;
  passwordRepeat: string;
};

export const initialValues: IRecoveryPasswordInitialValues = {
  password: "",
  passwordRepeat: "",
};

export const validationSchema = Yup.object().shape({
  password: Yup.string().required("auth.error.login"),
  passwordRepeat: Yup.string()
    .required()
    .test("validate password", "recovery.error", function (passwordRepeat) {
      const { password } = this.parent;
      return password === passwordRepeat;
    }),
});
