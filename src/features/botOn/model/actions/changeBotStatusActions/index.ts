import { createAsyncThunk } from "@reduxjs/toolkit";
import type { AsyncThunkConfig } from "providers";
import { BotStatus } from "shared/constants/botStatus";
import { changeBotStatusApi } from "../../api/changeBotStatus";
import type { IChangeBotStatusDto } from "../../api/changeBotStatus/index.types";

export const changeBotStatusActions = createAsyncThunk<
  IChangeBotStatusDto,
  IChangeBotStatusDto,
  AsyncThunkConfig
>("bot/settings/status", async (changeBotStatusDto, { rejectWithValue, getState }) => {
  try {
    await changeBotStatusApi(changeBotStatusDto);
    const { isCreated } = getState().botReducer.botInformation;
    if (!isCreated) return { status: BotStatus.CREATE };
    return changeBotStatusDto;
  } catch (e: any) {
    return rejectWithValue(e);
  }
});
