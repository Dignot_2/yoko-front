import { IStore } from "src/providers";

export const botStatusSelector = (state: IStore) => state.botReducer.botInformation.status;
