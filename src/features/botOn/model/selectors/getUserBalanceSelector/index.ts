import { IStore } from "src/providers";

export const getUserBalanceSelector = (state: IStore) => state.userReduser.user.balance;
