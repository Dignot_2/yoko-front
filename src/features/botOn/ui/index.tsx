import { useTranslation } from "react-i18next";
import ButtonLib from "@mui/material/Button";
import { useAppSelector, useAppDispatch } from "providers";
import { BotStatus } from "shared/constants/botStatus";
import { changeBotStatusActions, botStatusSelector, getUserBalanceSelector } from "../";

const BotOn = () => {
  const { t } = useTranslation();
  const dispatch = useAppDispatch();

  const status = useAppSelector(botStatusSelector);
  const balance = useAppSelector(getUserBalanceSelector);

  const handleBotOn = () => {
    dispatch(changeBotStatusActions({ status: BotStatus.ON }));
  };

  const disabled =
    status === BotStatus.ON ||
    balance <= 0 ||
    status === BotStatus.CREATE ||
    status === BotStatus.ERROR;

  return (
    <ButtonLib
      sx={{
        color:
          "#181938; height: 45px; text-transform: none; font-style: normal; font-weight: 500; font-size: 14px; line-height: 12px; letter-spacing: 0.3px;",
      }}
      fullWidth
      variant="text"
      size="small"
      onClick={() => handleBotOn()}
      {...{ disabled }}
    >
      <p>{t("bot.card.button.on")}</p>
    </ButtonLib>
  );
};

export default BotOn;
