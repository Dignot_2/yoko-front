import styled from "@emotion/styled";

export const ConfirmationUserWrapper = styled.div`
  display: flex;
  flex-flow: column;
  align-items: center;
  justify-content: center;
  gap: 34px;
  width: 100%;
`;

export const ConfirmationUserTitle = styled.h1`
  font-style: normal;
  font-weight: 600;
  font-size: 40px;
  line-height: 48px;
  text-align: center;
  color: #181938;

  @media screen and (max-width: 670px) {
    font-size: 30px;
    line-height: 32px;
  }
`;

export const ConfirmationUserError = styled.p`
  font-size: 16px;
  font-weight: 400;
  line-height: 24px;
  letter-spacing: 0.30000001192092896px;
  text-align: center;
  color: #181938;
`;
