import { createPortal } from "react-dom";
import { useSearchParams } from "react-router-dom";
import { useAppDispatch } from "providers";
import ConfirmationUserModal from "./confirmationUserModal";

const ConfirmationUser = () => {
  const dispatch = useAppDispatch();

  const [searchParams] = useSearchParams();

  const code = searchParams.get("code");

  if (code) {
    return createPortal(<ConfirmationUserModal {...{ code }} />, document.body);
  }
  return null;
};

export default ConfirmationUser;
