import http from "shared/api/axios";
import type { IConfirmationUserDto } from "./index.types";

export const confirmationUserApi = ({ code }: IConfirmationUserDto) =>
  http.get<undefined>(`user/confirmation/${code}`);
