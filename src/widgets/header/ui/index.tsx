import { isDesktop, isTablet } from "react-device-detect";
import Logo from "entities/logo";
import UserCard from "entities/userCard";
import LanguageSelectionMenu from "entities/languageSelectionMenu";
import { useAuth } from "app/hooks";
import * as Styled from "./index.styled";

const Header = () => {
  const isAuth = useAuth();

  return (
    <Styled.HeaderWrapper>
      <Logo type="home" />
      <Styled.HeaderActionsWrapper>
        <Styled.HeaderActionsWrapper>
          {isAuth && (isDesktop || isTablet) && <UserCard />}
          <LanguageSelectionMenu />
        </Styled.HeaderActionsWrapper>
      </Styled.HeaderActionsWrapper>
    </Styled.HeaderWrapper>
  );
};

export default Header;
