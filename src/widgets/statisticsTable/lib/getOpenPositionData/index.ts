import {
  GetPositionForOpenReponse,
  OpenPositionData,
} from "widgets/statisticsTable/api/getOpenPositions";
import { getFormatDate } from "shared/lib/getFormatDate";
import { ITableProps } from "shared/ui/Table";

export const getOpenPositionData = (
  positionsResponse: GetPositionForOpenReponse,
  handleOpenPositionModal: (selectPositions: OpenPositionData["positions"], symbol: string) => void
): Pick<ITableProps, "headCells" | "rows" | "pageCount"> => {
  const { count, data } = positionsResponse;

  const headCells = [
    {
      width: "20%",
      content: "table.statistics.active.head.date",
      position: "left",
      flexPosition: "flex-start",
    },
    {
      width: "20%",
      content: "table.statistics.active.head.symbol",
      position: "left",
      flexPosition: "flex-start",
    },
    {
      width: "20%",
      content: "table.statistics.active.head.size",
      position: "left",
      flexPosition: "flex-start",
    },
    {
      width: "20%",
      content: "table.statistics.active.head.count",
      position: "left",
      flexPosition: "flex-start",
    },
    {
      width: "20%",
      content: "table.statistics.active.head.target",
      position: "left",
      flexPosition: "flex-start",
    },
  ];

  const rows: ITableProps["rows"] = data.map(
    ({ position: { openingTime, symbol, size, targetPrice }, positions }) => ({
      ...(positions.length
        ? {
            handleClick: () => handleOpenPositionModal(positions, symbol),
          }
        : {}),
      mobileCard: {
        tableBlock: [
          {
            type: "text",
            title: "table.statistics.active.head.date",
            value: getFormatDate(openingTime),
            position: "left",
            isNotTranslate: true,
          },
          {
            type: "text",
            title: "table.statistics.active.head.symbol",
            value: symbol,
            position: "left",
            isNotTranslate: true,
          },
          {
            type: "text",
            title: "table.statistics.active.head.size",
            value: `${size}`,
            position: "left",
            isNotTranslate: true,
          },
          {
            type: "text",
            title: "table.statistics.active.head.count",
            value: `${positions.length}`,
            position: "left",
            isNotTranslate: true,
          },
          {
            type: "text",
            title: "table.statistics.active.head.target",
            value: `${targetPrice}`,
            position: "left",
            isNotTranslate: true,
          },
        ],
      },
      cells: [
        {
          width: "20%",
          content: getFormatDate(openingTime),
          position: "left",
          type: "text",
          flexPosition: "flex-start",
          isNotTranslate: true,
        },
        {
          width: "20%",
          content: symbol,
          position: "left",
          type: "text",
          flexPosition: "flex-start",
          isNotTranslate: true,
        },
        {
          width: "20%",
          content: `${size}`,
          position: "left",
          type: "text",
          flexPosition: "flex-start",
          isNotTranslate: true,
        },
        {
          width: "20%",
          content: `${positions.length}`,
          position: "left",
          type: "text",
          flexPosition: "flex-start",
          isNotTranslate: true,
        },
        {
          width: "20%",
          content: `${targetPrice}`,
          position: "left",
          type: "text",
          flexPosition: "flex-start",
          isNotTranslate: true,
        },
      ],
    })
  );

  return {
    pageCount: Math.ceil(count / 10),
    headCells,
    rows,
  };
};
