import { ITableProps } from "shared/ui/Table";
import { getFormatPeriod } from "shared/lib/getFormatPeriod";
import { GetPositionForHistoryReponse, HistoryPositionData } from "../../api/getHistoryPositons";

export const getHistoryPositionData = (
  positionsResponse: GetPositionForHistoryReponse,
  handleOpenPositionModal: (
    selectPositions: HistoryPositionData["positions"],
    symbol: string
  ) => void
): Pick<ITableProps, "headCells" | "rows" | "pageCount"> => {
  const { count, data } = positionsResponse;

  const headCells = [
    {
      width: "25%",
      content: "table.statistics.history.head.period",
      position: "left",
      flexPosition: "flex-start",
    },
    {
      width: "10%",
      content: "table.statistics.history.head.symbol",
      position: "left",
      flexPosition: "flex-start",
    },
    {
      width: "10%",
      content: "table.statistics.history.head.size",
      position: "left",
      flexPosition: "flex-start",
    },
    {
      width: "10%",
      content: "table.statistics.history.head.count",
      position: "left",
      flexPosition: "flex-start",
    },
    {
      width: "15%",
      content: "table.statistics.history.head.target.price",
      position: "left",
      flexPosition: "flex-start",
    },
    {
      width: "15%",
      content: "table.statistics.history.head.commision",
      position: "left",
      flexPosition: "flex-start",
    },
    {
      width: "15%",
      content: "table.statistics.history.head.profit",
      position: "left",
      flexPosition: "flex-start",
    },
  ];

  const rows: ITableProps["rows"] = data.map(
    ({ position: { period, symbol, size, commissions, profit, closingPrice }, positions }) => ({
      ...(positions.length
        ? {
            handleClick: () => handleOpenPositionModal(positions, symbol),
          }
        : {}),
      mobileCard: {
        tableBlock: [
          {
            type: "text",
            title: "table.statistics.history.head.period",
            value: getFormatPeriod(period),
            position: "left",
            isNotTranslate: true,
          },
          {
            type: "text",
            title: "table.statistics.history.head.symbol",
            value: symbol,
            position: "left",
            isNotTranslate: true,
          },
          {
            type: "text",
            title: "table.statistics.history.head.size",
            value: `${size}`,
            position: "left",
            isNotTranslate: true,
          },
          {
            type: "text",
            title: "table.statistics.history.head.count",
            value: `${positions.length}`,
            position: "left",
            isNotTranslate: true,
          },
          {
            type: "text",
            title: "table.statistics.history.head.target.price",
            value: `${closingPrice}`,
            position: "left",
            isNotTranslate: true,
          },
          {
            type: "text",
            title: "table.statistics.history.head.commision",
            value: `${commissions}`,
            position: "left",
            isNotTranslate: true,
          },
          {
            type: "text",
            title: "table.statistics.history.head.profit",
            value: `${profit}`,
            position: "left",
            isNotTranslate: true,
          },
        ],
      },
      cells: [
        {
          width: "25%",
          content: getFormatPeriod(period),
          position: "left",
          type: "text",
          flexPosition: "flex-start",
          isNotTranslate: true,
        },
        {
          width: "10%",
          content: symbol,
          position: "left",
          type: "text",
          flexPosition: "flex-start",
          isNotTranslate: true,
        },
        {
          width: "10%",
          content: `${size}`,
          position: "left",
          type: "text",
          flexPosition: "flex-start",
          isNotTranslate: true,
        },
        {
          width: "10%",
          content: `${positions.length}`,
          position: "left",
          type: "text",
          flexPosition: "flex-start",
          isNotTranslate: true,
        },
        {
          width: "15%",
          content: `${closingPrice}`,
          position: "left",
          type: "text",
          flexPosition: "flex-start",
          isNotTranslate: true,
        },
        {
          width: "15%",
          content: `${commissions}`,
          position: "left",
          type: "text",
          flexPosition: "flex-start",
          isNotTranslate: true,
        },
        {
          width: "15%",
          content: `${profit}`,
          position: "left",
          type: "text",
          flexPosition: "flex-start",
          isNotTranslate: true,
        },
      ],
    })
  );

  return {
    pageCount: Math.ceil(count / 10),
    headCells,
    rows,
  };
};
