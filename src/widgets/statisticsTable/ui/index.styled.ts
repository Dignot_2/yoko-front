import styled from "@emotion/styled";

export const StatisticsTableWrapper = styled.div`
  display: flex;
  flex-flow: column nowrap;
  align-items: flex-start;
  justify-content: flex-start;
  gap: 24px;
  width: 100%;
  height: 100%;
  overflow-y: hidden;
`;
