import styled from "@emotion/styled";

export const HistoryPositionModalWrapper = styled.div`
  padding: 56px;
  width: 100%;
  height: 100%;
  display: flex;
  flex-flow: column;
  align-items: center;
  justify-content: flex-start;
  gap: 32px;

  @media screen and (max-width: 450px) {
    padding: 24px;
    margin-bottom: 40px;
  }
`;

export const HistoryPositionModalInformationBlock = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const HistoryPositionModalInformationTitle = styled.h1`
  font-style: normal;
  font-weight: 600;
  font-size: 30px;
  line-height: 32px;
  color: #181938;

  @media screen and (max-width: 450px) {
    font-style: normal;
    font-weight: 600;
    font-size: 20px;
    line-height: 24px;
  }
`;

export const HistoryPositionModalInformationDescription = styled.h1`
  font-style: normal;
  font-weight: 400;
  font-size: 16px;
  line-height: 20px;
  letter-spacing: 0.3px;
  color: #9298b8;

  @media screen and (max-width: 450px) {
    font-style: normal;
    font-weight: 400;
    font-size: 16px;
    line-height: 20px;
  }
`;
