import { ITableProps } from "shared/ui/Table";
import { getFormatDate } from "shared/lib/getFormatDate";
import { OpenPositionData } from "../../../../api/getOpenPositions";

export const getTableData = (
  selectPositions: OpenPositionData["positions"]
): Pick<ITableProps, "headCells" | "rows" | "rowFlexPosition"> => {
  const headCells = [
    {
      width: "25%",
      content: "table.statistics.active.modal.head.date",
      position: "center",
      flexPosition: "flex-start",
    },
    {
      width: "20%",
      content: "table.statistics.active.modal.head.size",
      position: "center",
      flexPosition: "flex-start",
    },
    {
      width: "20%",
      content: "table.statistics.active.modal.head.openingPrice",
      position: "center",
      flexPosition: "flex-start",
    },
  ];

  const rows: ITableProps["rows"] = selectPositions.map(({ openingPrice, size, openingTime }) => ({
    mobileCard: {
      tableBlock: [
        {
          type: "text",
          title: "table.statistics.active.modal.head.date",
          value: getFormatDate(openingTime),
          position: "left",
          isNotTranslate: true,
        },
        {
          type: "text",
          title: "table.statistics.active.modal.head.size",
          value: `${size}`,
          position: "left",
        },
        {
          type: "text",
          title: "table.statistics.active.modal.head.openingPrice",
          value: `${openingPrice}`,
          position: "left",
        },
      ],
    },
    cells: [
      {
        width: "25%",
        content: getFormatDate(openingTime),
        position: "center",
        isNotTranslate: true,
        type: "text",
        flexPosition: "flex-start",
      },
      {
        width: "20%",
        content: `${size}`,
        position: "center",
        isNotTranslate: true,
        type: "text",
        flexPosition: "flex-start",
      },
      {
        width: "20%",
        content: `${openingPrice}`,
        position: "center",
        isNotTranslate: true,
        type: "text",
        flexPosition: "flex-start",
      },
    ],
  }));

  return {
    headCells,
    rows,
    rowFlexPosition: "space-between",
  };
};
