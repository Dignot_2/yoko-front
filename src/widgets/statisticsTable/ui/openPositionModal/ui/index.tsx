import { useTranslation } from "react-i18next";
import ModalPage from "shared/ui/ModalPage";
import Table from "shared/ui/Table";
import { OpenPositionData } from "../../../api/getOpenPositions";
import { getTableData } from "../lib/getTableData";
import * as Styled from "./index.styled";

type IOpenPositionModalProps = {
  symbol: string;
  selectPositions: OpenPositionData["positions"];
  onClose: () => void;
};

const OpenPositionModal = (props: IOpenPositionModalProps) => {
  const { t } = useTranslation();

  const { selectPositions, onClose, symbol } = props;

  if (!selectPositions?.length) return null;

  const tableData = getTableData(selectPositions);

  return (
    <ModalPage {...{ onClose }}>
      <Styled.OpenPositionModalWrapper>
        <Styled.OpenPositionModalInformationBlock>
          <Styled.OpenPositionModalInformationTitle>
            {t("table.statistics.active.modal.title")}
          </Styled.OpenPositionModalInformationTitle>
          <Styled.OpenPositionModalInformationDescription>
            {symbol}
          </Styled.OpenPositionModalInformationDescription>
        </Styled.OpenPositionModalInformationBlock>
        <Table {...tableData} />
      </Styled.OpenPositionModalWrapper>
    </ModalPage>
  );
};

export default OpenPositionModal;
