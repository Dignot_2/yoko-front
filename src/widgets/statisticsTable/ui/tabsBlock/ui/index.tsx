import { useTranslation } from "react-i18next";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import { tabs } from "../config";
import * as Styled from "./index.styled";
import { ITabsBlockProps } from "../index.types";

const TabsBlock = (props: ITabsBlockProps) => {
  const { t } = useTranslation();

  const { choiceValue, handleChangeTab } = props;

  const handleChange = (_, newValue: string) => {
    handleChangeTab(newValue);
  };

  return (
    <Styled.TabBlockWrapper>
      <Tabs value={choiceValue} onChange={handleChange}>
        {tabs.map(({ label: tabLabel, value }) => {
          const label = t(tabLabel);
          return <Tab {...{ label, value }} />;
        })}
      </Tabs>
    </Styled.TabBlockWrapper>
  );
};

export default TabsBlock;
