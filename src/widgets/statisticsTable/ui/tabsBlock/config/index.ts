import type { ITabs } from "../index.types";

export const tabs: ITabs[] = [
  {
    label: "table.statistics.tab.active.positions",
    value: "open",
  },
  {
    label: "table.statistics.tab.history.positions",
    value: "history",
  },
];
