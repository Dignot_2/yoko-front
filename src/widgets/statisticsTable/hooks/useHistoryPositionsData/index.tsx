import { useState, useEffect } from "react";
import {
  getHistoryPositions,
  GetPositionForHistoryReponse,
  HistoryPositionData,
} from "../../api/getHistoryPositons";
import { getHistoryPositionData } from "../..//lib/getHistoryPositionData";

type IUseHistoryPositionsData = {
  handleOpenPositionModal: (
    selectPositions: HistoryPositionData["positions"],
    symbol: string
  ) => void;
};

export const useHistoryPositionsData = (props: IUseHistoryPositionsData) => {
  const { handleOpenPositionModal } = props;

  const [historyPositionsData, setHistoryPositionsData] =
    useState<GetPositionForHistoryReponse | null>(null);

  const [activePage, setActivePage] = useState(0);
  const [isLoading, setIsLoading] = useState(false);

  const handleChangeActivePage = (num) => {
    setActivePage(num - 1);
  };

  useEffect(() => {
    setIsLoading(true);
    getHistoryPositions(10, activePage)
      .then((data) => setHistoryPositionsData(data))
      .catch(() => {
        setHistoryPositionsData(null);
      })
      .finally(() => setIsLoading(false));
  }, [activePage]);

  if (!historyPositionsData?.data?.length) {
    return {
      emptyMessage: "table.statistics.history.empty.message",
      isLoading,
    };
  }

  return {
    title: "table.statistics.tab.history.title",
    prefix: `${historyPositionsData.profit} USDT`,
    handleChangeActivePage,
    activePage,
    isLoading,
    ...getHistoryPositionData(historyPositionsData, handleOpenPositionModal),
  };
};
