import http from "shared/api/axios";
import type { IReferralData, IGetReferralParams } from "./index.types";

export const getReferralApi = ({ pageNumber, count }: IGetReferralParams): Promise<IReferralData> =>
  http.get<IReferralData>(`user/referral/${count}/${pageNumber}`);

export type { IReferralData };
