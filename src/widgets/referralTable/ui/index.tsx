import Table from "shared/ui/Table";
import { useReferralTableData } from "../lib/useReferralTableData";

const ReferralTable = () => {
  const tableData = useReferralTableData();

  return <Table {...tableData} />;
};

export default ReferralTable;
