import { useState, useEffect } from "react";
import { getReferralApi, IReferralData } from "../../model/api/getReferralApi";
import { getReferralData } from "../getReferralData";

export const useReferralTableData = () => {
  const [activePage, setActivePage] = useState(0);
  const [referralData, setReferralData] = useState<IReferralData | null>(null);
  const [isLoading, setIsLoading] = useState(false);

  const handleChangeActivePage = (num) => {
    setActivePage(num - 1);
  };

  useEffect(() => {
    setIsLoading(true);
    getReferralApi({
      count: 10,
      pageNumber: activePage,
    })
      .then((data) => setReferralData(data))
      .catch(() => {
        setReferralData(null);
      })
      .finally(() => setIsLoading(false));
  }, [activePage]);

  if (!referralData?.data?.length) {
    return {
      emptyMessage: "table.referral.empty.text",
      isLoading,
    };
  }

  return {
    title: "table.referral.title",
    handleChangeActivePage,
    activePage,
    isLoading,
    ...getReferralData(referralData),
  };
};
