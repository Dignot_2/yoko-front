import { useTranslation } from "react-i18next";
import { IRenderComponentProps } from "features/userAuth";
import Button from "shared/ui/Button/Button";

const RegisterButton = (props: IRenderComponentProps) => {
  const { handleClick } = props;
  const { t } = useTranslation();

  return (
    <Button size="small" variant="contained" color="secondary" onClick={() => handleClick()}>
      {t("button.reg")}
    </Button>
  );
};

export default RegisterButton;
