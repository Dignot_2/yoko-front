interface HeaderMenuItems {
  title: string;
}

export const headerMenuItems: HeaderMenuItems[] = [
  { title: "link.profit" },
  { title: "link.robot" },
  { title: "link.referral_program" },
  { title: "link.questions" },
  { title: "link.about" },
];
