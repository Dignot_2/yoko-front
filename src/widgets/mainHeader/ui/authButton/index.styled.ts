import styled from "@emotion/styled";

export const AuthButtonWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 10px;
`;

export const AuthButtonText = styled.p`
  font-style: normal;
  font-weight: 600;
  font-size: 15px;
  line-height: 14px;
  color: #5f5cec;
`;

export const AuthButtonIconBlock = styled.div`
  width: 16px;
  height: 18px;
`;
