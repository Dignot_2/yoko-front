import HomePageWrapper from "entities/HomePageWrapper";
import { getStatisticsData } from "../config";

const StatisticsWidget = () => {
  const statisticsData = getStatisticsData();
  return <HomePageWrapper {...statisticsData} />;
};

export default StatisticsWidget;
