import { useTranslation } from "react-i18next";
import { isDesktop } from "react-device-detect";
import * as Styled from "./index.styled";

const HomeFooter = () => {
  const { t } = useTranslation();

  if (isDesktop) {
    return (
      <Styled.FooterWrapper>
        <Styled.FooterText>{t("text.terms_of_use")}</Styled.FooterText>
        <Styled.FooterText>{t("text.privacy_policy")}</Styled.FooterText>
      </Styled.FooterWrapper>
    );
  }

  return null;
};

export default HomeFooter;
