import { createAsyncThunk } from "@reduxjs/toolkit";
import type { AsyncThunkConfig } from "src/providers/store";
import { getBotSettingsApi, IBotSettingsData } from "../../api/getBotSettingsApi";

export const getBotSettingsAction = createAsyncThunk<IBotSettingsData, undefined, AsyncThunkConfig>(
  "bot/get/settings",
  async (_, { rejectWithValue }) => {
    try {
      return await getBotSettingsApi();
    } catch (e: any) {
      return rejectWithValue(e);
    }
  }
);
