import { IChangeBotKeysData } from "features/changeBotKeys";
import { IBotSettingsData as IChangeBotSettingsData } from "features/changeBotSettings";

export type IBotSettingsData = IChangeBotKeysData & IChangeBotSettingsData;
