import http from "shared/api/axios";
import type { IBotSettingsData } from "./index.types";

export const getBotSettingsApi = () => http.get<IBotSettingsData>("bot/settings");
export type { IBotSettingsData };
