import { IStore } from "providers";

export const getChangeSettingsLoadingSelector = (state: IStore) => state.botReducer.loading;
