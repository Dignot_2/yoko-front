import { IStore } from "providers";

export const getBotKeysSettingsSelector = (state: IStore) => state.botReducer.keys;
