import { IStore } from "providers";

export const getBotSettingsSelector = (state: IStore) => state.botReducer.settings;
