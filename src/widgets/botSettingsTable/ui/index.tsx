import { useEffect } from "react";
import { useAppSelector, useAppDispatch } from "providers";
import ChangeBotKeys from "features/changeBotKeys";
import ChangeBotSettings from "features/changeBotSettings";
import Loading from "shared/ui/Loading/Loading";
import { getBotSettingsAction } from "../model/actions/getBotSettingsActions";
import { getBotKeysSettingsSelector } from "../model/selectors/getBotKeysSettingsSelector";
import { getBotSettingsSelector } from "../model/selectors/getBotSettingsSelector";
import { getChangeSettingsLoadingSelector } from "../model/selectors/getChangeSettingsLoadingSelector";

import * as Styled from "./index.styled";

const BotSettingsTable = () => {
  const dispatch = useAppDispatch();

  const botKeysSettings = useAppSelector(getBotKeysSettingsSelector);
  const botSettings = useAppSelector(getBotSettingsSelector);
  const isLoading = useAppSelector(getChangeSettingsLoadingSelector);

  useEffect(() => {
    dispatch(getBotSettingsAction());
  }, []);

  if (isLoading || !botKeysSettings || !botSettings) {
    return (
      <Styled.BotSettingsTableWrapper>
        <Loading />
      </Styled.BotSettingsTableWrapper>
    );
  }

  return (
    <Styled.BotSettingsTableWrapper>
      <ChangeBotKeys />
      <ChangeBotSettings />
    </Styled.BotSettingsTableWrapper>
  );
};

export default BotSettingsTable;
