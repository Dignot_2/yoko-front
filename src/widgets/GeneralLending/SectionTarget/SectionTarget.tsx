import { useTranslation } from "react-i18next";
import UserAuth from "features/userAuth";
import RuporImagePath from "shared/image/RuporImage.png";

const SectionTarget = () => {
  const { t } = useTranslation();

  return (
    <section className="target">
      <div className="container">
        <div className="target__wrapper">
          <h2 className="section-header target__header">
            {t("main.target.your.goal.profit.will.help.this")}
          </h2>
          <div className="target__up-blocks">
            <div className="block_item target__block">
              <div className="block_item_image target__image brain"></div>
              <div className="block_item-texts terget-item-texts">
                <div className="block_item_title target__block_header">
                  {t("main.target.smart.trading.algorithm")}
                </div>
                <div className="block_item_description target_item_description">
                  {t(
                    "main.target.combined.trading.strategies.trading.combined.together.efficiency.strategy"
                  )}
                  <br />
                  <UserAuth
                    type="auth"
                    renderComponent={({ handleClick }) => (
                      <div onClick={() => handleClick()} className="target__link">
                        {t("main.target.learn.more.strategy")}
                      </div>
                    )}
                  />
                </div>
              </div>
            </div>

            <div className="block_item target__block">
              <div className="block_item_image target__image coins"></div>
              <div className="block_item-texts terget-item-texts">
                <div className="block_item_title target__block_header">
                  {t("main.target.unlimited.list.coins")}
                </div>
                <div className="block_item_description target_item_description">
                  {t("main.target.robot.simultaneously.unlimited.automatically.trading.pairs")}
                </div>
              </div>
            </div>
            <div className="block_item target__block">
              <div className="block_item_image target__image mech"></div>
              <div className="block_item-texts terget-item-texts">
                <div className="block_item_title target__block_header">
                  {t("main.target.large.selection.settings")}
                </div>
                <div className="block_item_description target_item_description">
                  {t(
                    "main.target.capabilities.control.panel.trading.settings.effectively.individually.robot.improve.trading"
                  )}
                </div>
              </div>
            </div>
          </div>
          <div className="target__down-blocks">
            <div className="block_item target__down-block">
              <div className="block_item_image target__image super-bot">
                <div className="targer__image_green-block">{t("main.target.soon")}</div>
              </div>
              <div className="block_item-texts terget-item-texts">
                <div className="block_item_title target__block_header">
                  {t("main.target.telegram.bot")}
                </div>
                <div className="block_item_description target_item_description">
                  {t("main.target.connect.telegram.account.notify.transactions")}
                </div>
              </div>
            </div>
            <div className="block_item target__down-block">
              <div className="block_item_image target__image degree">
                <div className="targer__image_green-block">{t("main.target.soon")}</div>
              </div>
              <div className="block_item-texts terget-item-texts">
                <div className="block_item_title target__block_header">
                  {t("main.target.simulation.mode")}
                </div>
                <div className="block_item_description target_item_description target__block-descr">
                  {t(
                    "main.target.personal.account.simulation.mode.deposit.traded.period.yoko.robot"
                  )}
                </div>
              </div>
            </div>
          </div>
          <UserAuth
            type="auth"
            renderComponent={({ handleClick }) => (
              <div className="button target__button" onClick={() => handleClick()}>
                {t("main.target.start.trading")}
              </div>
            )}
          />
        </div>
        <div className="referal">
          <div className="referal__block">
            <h2 className="section-header referal-header">{t("main.target.referral.program")}</h2>
            <div className="section-subheader">{t("main.target.invite.friends.earn")}</div>
            <div className="block_item_description referal__desscription">
              {t(
                "main.target.affiliate.program.profit.commission.withdraws.percentage.referral.crypto.profit.commission.withdraws.referral.wallet"
              )}
            </div>
            <UserAuth
              type="auth"
              renderComponent={({ handleClick }) => (
                <div className="button referal__button" onClick={() => handleClick()}>
                  {t("main.target.become.referral")}
                </div>
              )}
            />
          </div>
          <div className="block_image referal-block__image">
            <img src={RuporImagePath} className="referal__image" />
          </div>
        </div>
      </div>
    </section>
  );
};

export default SectionTarget;
