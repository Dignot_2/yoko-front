import styled from "@emotion/styled";

export const CardTransactionWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  max-width: 256px;
  background: #ffffff;
  box-shadow: 0 10px 20px rgba(20, 25, 143, 0.05);
  border-radius: 5px;
  padding: 20px;
  margin-bottom: 10px;

  .sum {
    font-family: "Roboto", sans-serif;
    font-style: normal;
    font-weight: 900;
    font-size: 30px;
    line-height: 35px;
    margin-bottom: 8px;

    color: #181938;
  }
  .exchange {
    font-family: "Roboto", sans-serif;
    font-style: normal;
    font-weight: 600;
    font-size: 13px;
    line-height: 14px;
    padding: 3px 10px;
    color: #ffffff;
    background: #05cd99;
    border-radius: 15px;
    margin-bottom: 20px;
  }
  .date-container {
    display: flex;
    justify-content: flex-start;
    align-items: center;
    gap: 10px;
    background: #f4f7fe;
    border-radius: 5px;
    padding: 10px 15px;
  }
  .date-time-img {
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .date-title {
    font-family: "Roboto", sans-serif;
    font-style: normal;
    font-weight: 500;
    font-size: 12px;
    line-height: 12px;
    letter-spacing: 0.3px;
    padding-bottom: 5px;
    color: #5e658d;
  }
  .date-time {
    font-family: "Roboto", sans-serif;
    font-style: normal;
    font-weight: 800;
    font-size: 14px;
    line-height: 16px;

    color: #181938;
  }
`;
