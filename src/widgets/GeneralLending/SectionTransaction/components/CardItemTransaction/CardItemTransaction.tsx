import { useTranslation } from "react-i18next";
import CalandarIcon from "../../../assets/icons/signs/Calandar.svg";
import { CardTransactionWrapper } from "./CardItemTransaction.styled";

interface CardItemTransactionProps {
  sum: string;
  currency: string;
  exchange: string;
  date: string;
}

const CardItemTransaction = ({ sum, currency, exchange, date }: CardItemTransactionProps) => {
  const { t } = useTranslation();
  return (
    <>
      <CardTransactionWrapper>
        <div className="sum">
          {sum} {currency}
        </div>
        <p className="exchange">{exchange}</p>
        <div className="date-container">
          <div className="date-time-img">
            <CalandarIcon />
          </div>
          <div className="date-time-wrapper">
            <div className="date-title">{t("main.income.received")}</div>
            <div className="date-time">{date}</div>
          </div>
        </div>
      </CardTransactionWrapper>
    </>
  );
};

export default CardItemTransaction;
