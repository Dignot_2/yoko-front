import * as Styled from "./GeneralLending.styled";

import "./assets/css/blocks/header.css";
import "./assets/css/blocks/block_image.css";
import "./assets/css/blocks/buttons.css";
import "./assets/css/blocks/headers.css";
import "./assets/css/blocks/block_item.css";
import "./assets/css/blocks/footer.css";
import "./assets/css/blocks/overlay.css";
import "./assets/css/blocks/trial.css";
import "./assets/css/blocks/income.css";
import "./assets/css/blocks/transaction.css";
import "./assets/css/blocks/connect.css";
import "./assets/css/blocks/watch.css";
import "./assets/css/blocks/desk.css";
import "./assets/css/blocks/target.css";
import "./assets/css/blocks/referal.css";
import "./assets/css/blocks/choice.css";
import "./assets/css/blocks/blog.css";
import "./assets/css/blocks/enter.css";
import "./assets/css/blocks/hamburger.css";
import "./assets/css/style.css";
import SectionTrial from "./SectionTrial";
import SectionIncome from "./SectionIncome";
import SectionTransaction from "./SectionTransaction";
import SectionWatch from "./SectionWatch";
import SectionTarget from "./SectionTarget";
import SectionChoice from "./SectionChoice";
import SocialButton from "./SocialButton";

const GeneralLending = (): JSX.Element => {
  return (
    <Styled.MainLendingWrapper>
      <SocialButton />
      <SectionTrial />
      <SectionIncome />
      <SectionTransaction />
      <SectionWatch />
      <SectionTarget />
      <SectionChoice />
    </Styled.MainLendingWrapper>
  );
};

export default GeneralLending;
