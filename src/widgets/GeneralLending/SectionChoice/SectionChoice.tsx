import { useTranslation } from "react-i18next";
import Slider from "shared/ui/Slider";
import {
  CardChoiceClient,
  CardChoiceBlog,
  cardChoiceClientItem,
  settingSliderClient,
  settingSliderBlog,
} from "./components";
import ArrowChevronForward2Icon from "../assets/icons/signs/ArrowChevronForward-2.svg";
import ArrowChevronForwardIcon from "../assets/icons/signs/ArrowChevronForward.svg";
import * as Styled from "./SectionChoice.styled";

const SectionChoice = () => {
  const { t } = useTranslation();

  return (
    <section className="choice">
      <div className="container choice__container">
        <div className="choice__wrapper">
          <h2 className="section-header chioce__header">{t("main.choice.clients.choose.us")}</h2>
          <Styled.WrapperSlider>
            <div className="client-arrow-prev">
              <ArrowChevronForward2Icon />
            </div>
            <Slider
              settings={settingSliderClient}
              items={cardChoiceClientItem.map(() => (
                <CardChoiceClient />
              ))}
            />
            <div className="dotted-block-client">
              <div className="client-arrow-next">
                <ArrowChevronForwardIcon />
              </div>
              <Styled.PaginationWrapper className="paginationWrapperClient" />
            </div>
          </Styled.WrapperSlider>
          <div className="blog">
            <h2 className="section-header chioce__header">{t("main.choice.follow.our.blog")}</h2>
            <Styled.WrapperSlider>
              <div className="blog-arrow-prev">
                <ArrowChevronForward2Icon />
              </div>
              <Slider
                settings={settingSliderBlog}
                items={new Array(3).fill(0).map(() => (
                  <CardChoiceBlog />
                ))}
              />
              <div className="dotted-block-blog">
                <div className="blog-arrow-next">
                  <ArrowChevronForwardIcon />
                </div>
                <Styled.PaginationWrapper className="paginationWrapperBlog"></Styled.PaginationWrapper>
              </div>
            </Styled.WrapperSlider>
          </div>
        </div>
      </div>
    </section>
  );
};

export default SectionChoice;
