import { useTranslation } from "react-i18next";
import GirlImagePath from "./assets/img/girl.svg";
import * as Styled from "./CardChoiceClient.styled";

const CardChoiceClient = () => {
  const { t } = useTranslation();
  return (
    <Styled.WrapperClient>
      <div className="image">
        <GirlImagePath />
      </div>
      <div className="description">
        <i>{t("main.choice.example.riviews")}</i>
      </div>
    </Styled.WrapperClient>
  );
};

export default CardChoiceClient;
