import styled from "@emotion/styled";

export const WrapperClient = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  gap: 20px;
  padding: 30px;
  background: #ffffff;
  box-shadow: 0 10px 20px rgba(20, 25, 143, 0.05);
  border-radius: 5px;
  margin-bottom: 10px;
  @media (max-width: 480px) {
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
  }
`;
