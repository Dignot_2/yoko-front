import { IStore } from "providers";

export const getUserTradingBalanceSelector = (state: IStore) =>
  state.userReduser.user.tradingBalance;
