import { IStore } from "providers";

export const getBotErrorSelector = (state: IStore) => state.botReducer.botInformation?.error;
