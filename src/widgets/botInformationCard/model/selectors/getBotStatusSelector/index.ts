import { IStore } from "providers";

export const getBotStatusSelector = (state: IStore) => state.botReducer.botInformation?.status;
