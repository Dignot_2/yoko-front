import { useState, useEffect } from "react";
import { isDesktop } from "react-device-detect";
import { useNavigate, useLocation } from "react-router-dom";
import { useTranslation } from "react-i18next";
import MenuIcon from "@mui/icons-material/Menu";
import HomeIcon from "@mui/icons-material/Home";
import SignalCellularAltIcon from "@mui/icons-material/SignalCellularAlt";
import PeopleIcon from "@mui/icons-material/People";
import PercentIcon from "@mui/icons-material/Percent";
import UserCard from "entities/userCard";
import SocialBlock from "entities/socialBlock";
import { PATH } from "shared/constants/routes";
import * as Styled from "./index.styled";

const HomeSideBar = () => {
  const { t } = useTranslation();
  const [open, setOpen] = useState(isDesktop);
  const navigate = useNavigate();
  const location = useLocation();
  const path = location.pathname;

  const handleClickWidow = (ev: any) => {
    const clickedElement = ev.target as HTMLDivElement;
    const sidebar = document.getElementById("sidebar")!;
    const its_menu = clickedElement == sidebar || sidebar.contains(clickedElement);
    if (!its_menu && clickedElement?.parentElement?.id !== "languages-item") {
      setOpen(false);
    }
  };

  const handleNavigate = (path: string) => navigate(path);

  useEffect(() => {
    if (!isDesktop) {
      document.addEventListener("mousedown", handleClickWidow);
      return () => document.removeEventListener("mousedown", handleClickWidow);
    }
  }, [isDesktop]);

  if (open) {
    return (
      <Styled.HomeSideBarWrapper id="sidebar" isPosition={!isDesktop}>
        <Styled.HomeSideBarNavigationBlock>
          {!isDesktop && <UserCard isSideBar />}
          <Styled.NavigateElement
            selected={path === PATH.HOME}
            onClick={() => handleNavigate(PATH.HOME)}
          >
            <HomeIcon sx={{ color: "#CFCEF9" }} />
            <Styled.NavigateText>{t("navigate.home")}</Styled.NavigateText>
          </Styled.NavigateElement>
          <Styled.NavigateElement
            selected={path === PATH.POSITION}
            onClick={() => handleNavigate(PATH.POSITION)}
          >
            <SignalCellularAltIcon sx={{ color: "#CFCEF9" }} />
            <Styled.NavigateText>{t("navigate.position")}</Styled.NavigateText>
          </Styled.NavigateElement>
          <Styled.NavigateElement
            selected={path === PATH.REFERRAL}
            onClick={() => handleNavigate(PATH.REFERRAL)}
          >
            <PeopleIcon sx={{ color: "#CFCEF9" }} />
            <Styled.NavigateText>{t("navigate.referral")}</Styled.NavigateText>
          </Styled.NavigateElement>
          <Styled.NavigateElement
            selected={path === PATH.PAYMENT}
            onClick={() => handleNavigate(PATH.PAYMENT)}
          >
            <PercentIcon sx={{ color: "#CFCEF9" }} />
            <Styled.NavigateText>{t("navigate.payment")}</Styled.NavigateText>
          </Styled.NavigateElement>
        </Styled.HomeSideBarNavigationBlock>
        <Styled.HomeSideBarInfomationBlock>
          {!isDesktop && (
            <Styled.HomeSideBarInfomationTitle>
              {t("text.join_us")}
            </Styled.HomeSideBarInfomationTitle>
          )}
          <SocialBlock />
          {!isDesktop && (
            <>
              <Styled.HomeSideBarInfomationDescription>
                {t("text.privacy_policy")}
              </Styled.HomeSideBarInfomationDescription>
              <Styled.HomeSideBarInfomationDescription>
                {t("text.terms_of_use")}
              </Styled.HomeSideBarInfomationDescription>
            </>
          )}
        </Styled.HomeSideBarInfomationBlock>
      </Styled.HomeSideBarWrapper>
    );
  }

  return (
    <Styled.HomeMenuButton onClick={() => setOpen(true)}>
      <MenuIcon />
    </Styled.HomeMenuButton>
  );
};

export default HomeSideBar;
