import { ITableProps } from "shared/ui/Table";
import { getFormatDate } from "shared/lib/getFormatDate";
import { getStatusText } from "../getStatusText";
import type { IDepositData, ITransactions } from "../../model/api/getDepositApi";

type IGetTableDataProps = {
  handleOpenDepositModal: (transactions: ITransactions[], id: number) => void;
  depositData: IDepositData;
};

export const getTableData = (
  props: IGetTableDataProps
): Pick<ITableProps, "headCells" | "rows" | "pageCount"> => {
  const { depositData, handleOpenDepositModal } = props;
  const { count, data } = depositData;

  const headCells = [
    {
      width: "14.2%",
      content: "table.deposit.number",
      position: "center",
      flexPosition: "flex-start",
    },
    {
      width: "14.2%",
      content: "table.deposit.date",
      position: "center",
      flexPosition: "flex-start",
    },
    {
      width: "14.2%",
      content: "table.deposit.transactions",
      position: "center",
      flexPosition: "flex-start",
    },
    {
      width: "14.2%",
      content: "table.deposit.waiting",
      position: "center",
      flexPosition: "flex-start",
    },
    {
      width: "14.2%",
      content: "table.deposit.credited",
      position: "center",
      flexPosition: "flex-start",
    },
    {
      width: "14.2%",
      content: "table.deposit.status",
      position: "center",
      flexPosition: "flex-start",
    },
    {
      width: "14.2%",
      content: "table.deposit.link",
      position: "center",
      flexPosition: "flex-start",
    },
  ];

  const rows: ITableProps["rows"] = data.map(
    ({ id, createAt, transactionsIds, amount, received, link, status }) => {
      return {
        handleClick: transactionsIds?.length
          ? () => handleOpenDepositModal(transactionsIds, id)
          : undefined,
        mobileCard: {
          title: `№ ${id}`,
          description: getFormatDate(createAt),
          descriptionBlock: {
            type: "status",
            status,
            title: getStatusText(status),
          },
          tableBlock: [
            {
              type: "text",
              title: "table.deposit.transactions",
              variable: `${transactionsIds?.length || 0}`,
              value: "table.deposit.transactions.cell",
              color: "#5F5CEC",
              position: "left",
            },
            {
              type: "text",
              title: "table.deposit.waiting",
              value: `${amount} USDT`,
              position: "left",
              isNotTranslate: true,
            },
            {
              type: "text",
              title: "table.deposit.credited",
              value: `${received} USDT`,
              position: "left",
              isNotTranslate: true,
            },
            {
              type: "link",
              title: "table.deposit.link",
              value: link,
              position: "left",
              textLink: "table.deposit.linkText",
            },
          ],
        },
        cells: [
          {
            width: "14.2%",
            content: `№ ${id}`,
            position: "center",
            type: "text",
            flexPosition: "flex-start",
            isNotTranslate: true,
          },
          {
            width: "14.2%",
            content: getFormatDate(createAt),
            position: "center",
            type: "text",
            flexPosition: "flex-start",
            isNotTranslate: true,
          },
          {
            width: "14.2%",
            variable: `${transactionsIds?.length || 0}`,
            content: "table.deposit.transactions.cell",
            color: "#5F5CEC",
            position: "center",
            type: "text",
            flexPosition: "flex-start",
          },
          {
            width: "14.2%",
            content: `${amount} USDT`,
            position: "center",
            type: "text",
            flexPosition: "flex-start",
            isNotTranslate: true,
          },
          {
            width: "14.2%",
            content: `${received} USDT`,
            position: "center",
            type: "text",
            flexPosition: "flex-start",
            isNotTranslate: true,
          },
          {
            width: "14.2%",
            content: getStatusText(status),
            statusType: status,
            position: "center",
            type: "status",
            flexPosition: "flex-start",
          },
          {
            width: "14.2%",
            content: link,
            position: "center",
            type: "link",
            flexPosition: "flex-start",
            textLink: "table.deposit.linkText",
          },
        ],
      };
    }
  );

  return {
    pageCount: Math.ceil(count / 10),
    headCells,
    rows,
  };
};
