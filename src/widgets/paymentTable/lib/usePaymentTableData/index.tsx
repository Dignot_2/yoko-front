import { useState, useEffect } from "react";
import type { ITableProps } from "shared/ui/Table";
import { getDepositApi, IDepositData, ITransactions } from "../../model/api/getDepositApi";
import { getTableData } from "../getTableData";

type IUsePaymentTableDataProps = {
  handleOpenDepositModal: (transactions: ITransactions[], id: number) => void;
};

const usePaymentTableData = (props: IUsePaymentTableDataProps): ITableProps => {
  const { handleOpenDepositModal } = props;
  const [activePage, setActivePage] = useState(0);
  const [depositData, setDepositData] = useState<IDepositData | null>(null);
  const [isLoading, setIsLoading] = useState(false);

  const handleChangeActivePage = (num) => {
    setActivePage(num - 1);
  };

  useEffect(() => {
    setIsLoading(true);
    getDepositApi({
      count: 10,
      pageNumber: activePage,
    })
      .then((data) => setDepositData(data))
      .catch(() => {
        setDepositData(null);
      })
      .finally(() => setIsLoading(false));
  }, [activePage]);

  if (!depositData?.data?.length) {
    return {
      emptyMessage: "table.deposit.empty.text",
      isLoading,
    };
  }

  return {
    handleChangeActivePage,
    activePage,
    isLoading,
    ...getTableData({ depositData, handleOpenDepositModal }),
  };
};

export default usePaymentTableData;
