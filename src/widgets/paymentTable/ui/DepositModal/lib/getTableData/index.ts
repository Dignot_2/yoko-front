import { ITableProps } from "shared/ui/Table";
import { ITransactions } from "../../../../model/api/getDepositApi";

export const getTableData = (
  transactions: ITransactions[]
): Pick<ITableProps, "headCells" | "rows" | "rowFlexPosition"> => {
  const headCells = [
    {
      width: "25%",
      content: "table.transactions.date",
      position: "center",
      flexPosition: "flex-start",
    },
    {
      width: "20%",
      content: "table.transactions.price",
      position: "center",
      flexPosition: "flex-start",
    },
    {
      width: "20%",
      content: "table.transactions.currency",
      position: "center",
      flexPosition: "flex-start",
    },
  ];

  const rows: ITableProps["rows"] = transactions.map(({ id, createAt, currency, amount }) => ({
    mobileCard: {
      description: createAt,
      tableBlock: [
        {
          type: "text",
          title: "table.transactions.price",
          value: amount,
          position: "left",
        },
        {
          type: "text",
          title: "table.transactions.currency",
          value: currency,
          position: "left",
        },
      ],
    },
    cells: [
      {
        width: "25%",
        content: createAt,
        position: "center",
        isNotTranslate: true,
        type: "text",
        flexPosition: "flex-start",
      },
      {
        width: "20%",
        content: amount,
        position: "center",
        isNotTranslate: true,
        type: "text",
        flexPosition: "flex-start",
      },
      {
        width: "20%",
        content: currency,
        position: "center",
        isNotTranslate: true,
        type: "text",
        flexPosition: "flex-start",
      },
    ],
  }));

  return {
    headCells,
    rows,
    rowFlexPosition: "space-between",
  };
};
