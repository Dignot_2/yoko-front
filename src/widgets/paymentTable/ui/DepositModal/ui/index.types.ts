import type { IDeposit } from "../../../model/api/getDepositApi";

export type IDepositModalProps = Pick<IDeposit, "transactionsIds"> & {
  onClose: () => void;
  depositId: number | null;
};
