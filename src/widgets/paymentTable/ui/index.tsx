import { useState } from "react";
import { ITableComponentProps } from "entities/HomePageWrapper";
import Table from "shared/ui/Table";
import usePaymentTableData from "../lib/usePaymentTableData";
import { ITransactions } from "../model/api/getDepositApi";
import DepositModal from "./DepositModal";

const PaymentTable = (props: ITableComponentProps) => {
  const [selectedDeposit, setSelectedDeposit] = useState<ITransactions[] | null>(null);
  const [depositId, setDepositId] = useState<number | null>(null);

  const handleCloseDepositModal = () => {
    setSelectedDeposit(null);
  };

  const handleOpenDepositModal = (transactions: ITransactions[], id: number) => {
    setSelectedDeposit(transactions);
    setDepositId(id);
  };

  const tableData = usePaymentTableData({ ...{ ...props, handleOpenDepositModal } });

  return (
    <>
      <DepositModal
        transactionsIds={selectedDeposit}
        onClose={handleCloseDepositModal}
        {...{ depositId }}
      />
      <Table {...tableData} />
    </>
  );
};

export default PaymentTable;
