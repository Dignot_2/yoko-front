import { IStore } from "providers";

export const getUserReferralCodeSelector = (state: IStore) => state.userReduser.user.referralCode;
