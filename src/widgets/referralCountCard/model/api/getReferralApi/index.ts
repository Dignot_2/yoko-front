import http from "shared/api/axios";
import type { IReferralCountData } from "./index.types";

export const getReferralApi = () => http.get<IReferralCountData>("user/referral/count");
export type { IReferralCountData };
