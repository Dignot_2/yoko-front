import { IStore } from "providers";

export const getReferralCountLoadingSelector = (state: IStore) => state.referralReducer.loading;
