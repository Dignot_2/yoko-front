import { createAsyncThunk } from "@reduxjs/toolkit";
import type { AsyncThunkConfig } from "providers";
import { IReferralCountData, getReferralApi } from "../../api/getReferralApi";

export const getReferralCountAction = createAsyncThunk<
  IReferralCountData,
  undefined,
  AsyncThunkConfig
>("referral/count", async (_, { rejectWithValue }) => {
  try {
    return await getReferralApi();
  } catch (e: any) {
    return rejectWithValue(e);
  }
});
