import styled from "@emotion/styled";

export const BonusCardWrapper = styled.div`
  padding: 40px 40px;
  display: flex;
  flex-flow: column;
  align-items: center;
  justify-content: center;
  gap: 34px;
  width: 100%;

  @media screen and (max-width: 670px) {
    padding: 20px 20px;
  }
`;

export const BonusCardImage = styled.img`
  width: 137.5px;
  height: 137.5px;

  @media screen and (max-width: 670px) {
    width: 102.67px;
    height: 102.67px;
  }
`;

export const BonusCardTitle = styled.h1`
  font-style: normal;
  font-weight: 600;
  font-size: 40px;
  line-height: 48px;
  text-align: center;
  color: #181938;

  & > span {
    color: #5f5cec;
  }
  @media screen and (max-width: 670px) {
    font-style: normal;
    font-weight: 600;
    font-size: 30px;
    line-height: 32px;
    text-align: center;
    color: #181938;
  }
`;

export const BonusCardDecription = styled.h1`
  font-style: normal;
  font-weight: 400;
  font-size: 16px;
  line-height: 24px;
  text-align: center;
  letter-spacing: 0.3px;
  color: rgba(24, 25, 56, 0.4);
`;
