import { useState, useEffect, useRef } from "react";
import { useTranslation } from "react-i18next";
import UserAuth from "features/userAuth";
import Button from "shared/ui/Button/Button";
import ModalPage from "shared/ui/ModalPage";
import BonusImagePath from "shared/image/bonusImage.png";
import * as Styled from "./index.styled";

const BonusCard = () => {
  const { t } = useTranslation();
  const showBonusTimer = useRef(null);

  const [open, setIsOpen] = useState(false);

  const handleStartInterval = () => {
    showBonusTimer.current = setInterval(() => {
      const modalPage = document.getElementById("modal-page");
      if (setIsOpen && !modalPage) {
        setIsOpen(true);
      }
      if (modalPage) {
        clearInterval(showBonusTimer.current);
        handleStartInterval();
      }
    }, 10000);
  };

  useEffect(() => {
    handleStartInterval();
    return () => {
      clearInterval(showBonusTimer.current);
    };
  }, []);

  const handleClose = () => {
    setIsOpen(false);
    clearInterval(showBonusTimer.current);
  };

  if (!open) return null;

  return (
    <ModalPage onClose={() => handleClose()}>
      <Styled.BonusCardWrapper>
        <Styled.BonusCardImage src={BonusImagePath} alt="bonus image" />
        <Styled.BonusCardTitle>
          {t("text.bonus.title")} <span>10 USDT</span>
        </Styled.BonusCardTitle>
        <Styled.BonusCardDecription>{t("text.bonus.decsription")}</Styled.BonusCardDecription>
        <UserAuth
          type="register"
          renderComponent={({ handleClick }) => (
            <Button
              size="medium"
              variant="contained"
              color="primary"
              onClick={() => {
                handleClick();
              }}
            >
              {t("register.button.submit")}
            </Button>
          )}
        />
      </Styled.BonusCardWrapper>
    </ModalPage>
  );
};

export default BonusCard;
