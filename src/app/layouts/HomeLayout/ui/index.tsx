import Header from "widgets/header";
import SideBar from "widgets/homeSideBar";
import Footer from "widgets/homeFooter";
import * as Styled from "./index.styled";

const AppLayout = (props) => {
  const { children } = props;
  return (
    <Styled.AppLayoutWrapper>
      <Header />
      <Styled.ContentWrapper>
        <SideBar />
        <Styled.ChildWrapper>
          <Styled.PageWrapper>{children}</Styled.PageWrapper>
          <Styled.PageFooter>
            <Footer />
          </Styled.PageFooter>
        </Styled.ChildWrapper>
      </Styled.ContentWrapper>
    </Styled.AppLayoutWrapper>
  );
};

export default AppLayout;
