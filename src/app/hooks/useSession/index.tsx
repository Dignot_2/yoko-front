import { useEffect, useRef } from "react";
import { useAppDispatch } from "providers";
import { LOCAL_STORAGE_TOKEN_NAME } from "shared/constants/variable";
import { sessionApi } from "shared/api/user/session";
import { sessionUser } from "./model/actions/sessionUserActions";

const useSession = () => {
  const interval = useRef(null);
  const token = window.localStorage.getItem(LOCAL_STORAGE_TOKEN_NAME);

  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(sessionUser());

    if (token) {
      interval.current = setInterval(() => sessionApi(), 1000 * 60 * 12);
    }
    if (!token && interval.current) {
      clearInterval(interval.current);
    }
    return () => clearInterval(interval.current);
  }, [token]);
};

export default useSession;
