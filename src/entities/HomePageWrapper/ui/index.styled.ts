import styled from "@emotion/styled";

export const HomePageWrapper = styled.div`
  padding-top: 51px;
  position: relative;
  display: flex;
  flex-flow: column nowrap;
  align-items: flex-start;
  justify-content: flex-start;
  gap: 10px;
  box-sizing: border-box;
  min-height: 100%;
  width: 100%;
`;

type IIsMobileProps = {
  isMobile: boolean;
};

export const HomePageTitle = styled.h1`
  margin-bottom: 25px;
  font-style: normal;
  font-weight: 700;
  font-size: 34px;
  line-height: 34px;
  letter-spacing: 0.3px;
  color: #181938;
`;

export const HomePageCardsBlock = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  gap: 30px;
  margin-bottom: 19px;

  @media screen and (max-width: 1392px) {
    flex-flow: column nowrap;
    gap: 15px;
  }
`;

export const HomePageInformationWrapper = styled.div<IIsMobileProps>`
  padding: ${({ isMobile }) => (isMobile ? "0" : "24px")};
  height: fit-content;
  min-height: 100%;
  width: 100%;
  min-height: 100%;
  background: ${({ isMobile }) => (isMobile ? "none" : "#fff")};
  box-shadow: ${({ isMobile }) =>
    isMobile ? "none" : "0 25px 20px -25px rgba(20, 25, 143, 0.15)"};
  border-radius: 20px;
  box-sizing: border-box;
  display: flex;
  flex-flow: column nowrap;
  align-items: flex-start;
  justify-content: flex-start;
  gap: 10px;
`;
