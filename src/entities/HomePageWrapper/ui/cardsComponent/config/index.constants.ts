export enum CardsType {
  USER_MAIN_BALANCE = "user.main.balance",
  BOT_ACTIONS = "bot.actions",
  TRADING_BALANCE = "trading.balance",
  REFERRAL_BALANCE = "referral.balance",
  REFERRAL_COUNT = "referral.count",
  REFERRAL_CODE = "referral.code",
}
