export type ITableType = "PAYMENT" | "REFERRAL" | "BOT_SETTINGS" | "STATISTICS";

export type ITableComponentProps = {
  tableType: ITableType;
};
