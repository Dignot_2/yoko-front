import PaymentTable from "widgets/paymentTable";
import ReferralTable from "widgets/referralTable";
import BotSettingsTable from "widgets/botSettingsTable";
import StatisticsTable from "widgets/statisticsTable";
import type { ITableComponentProps } from "../index.types";

const TableComponent = (props: ITableComponentProps) => {
  const { tableType } = props;
  switch (tableType) {
    case "PAYMENT":
      return <PaymentTable {...props} />;
    case "REFERRAL":
      return <ReferralTable />;
    case "BOT_SETTINGS":
      return <BotSettingsTable />;
    case "STATISTICS":
      return <StatisticsTable />;
    default:
      return null;
  }
};

export default TableComponent;
