import { useTranslation } from "react-i18next";
import { isMobile } from "react-device-detect";
import TableComponent from "./tableComponent";
import CardsComponent from "./cardsComponent";
import ChartComponent from "./chartComponent";
import type { IHomePageWrapperProps } from "../index.types";
import * as Styled from "./index.styled";

const HomePageLayout = (props: IHomePageWrapperProps) => {
  const { title, tableType, cards, eChartsOption } = props;
  const { t } = useTranslation();

  return (
    <Styled.HomePageWrapper>
      <Styled.HomePageTitle>{t(title)}</Styled.HomePageTitle>
      <Styled.HomePageCardsBlock>
        {cards?.length && cards.map((type) => <CardsComponent {...{ type }} />)}
      </Styled.HomePageCardsBlock>
      <Styled.HomePageInformationWrapper {...{ isMobile }}>
        {eChartsOption && <ChartComponent {...eChartsOption} />}
        <TableComponent {...{ tableType }} />
      </Styled.HomePageInformationWrapper>
    </Styled.HomePageWrapper>
  );
};

export default HomePageLayout;
