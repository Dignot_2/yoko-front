import ReactEcharts, { EChartsOption } from "echarts-for-react";

const ChartComponent = (props: EChartsOption) => {
  return (
    <ReactEcharts option={...props} style={{ height: 500, width: "120%", alignSelf: "center" }} />
  );
};

export default ChartComponent;
