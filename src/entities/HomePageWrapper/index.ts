export { default } from "./ui";
export type { IHomePageWrapperProps } from "./index.types";
export { CardsType } from "./ui/cardsComponent/config/index.constants";
export type { ITableComponentProps, ITableType } from "./ui/tableComponent";
