import styled from "@emotion/styled";

export const LogoWrapper = styled.div`
  width: 97px;
  height: 60px;
`;

export const LogoImage = styled.img`
  object-fit: contain;
  width: 97px;
  height: 60px;
`;
