import LogoImage from "shared/image/Logo.png";
import WhiteLogo from "shared/image/WhiteLogo.png";
import type { ILogo } from "./index.types";
import * as Styled from "./index.styled";

const Logo = (props: ILogo) => {
  const { type } = props;
  return (
    <Styled.LogoWrapper>
      <Styled.LogoImage src={type === "home" ? LogoImage : WhiteLogo} alt="logotip Yoko Trade" />
    </Styled.LogoWrapper>
  );
};

export default Logo;
