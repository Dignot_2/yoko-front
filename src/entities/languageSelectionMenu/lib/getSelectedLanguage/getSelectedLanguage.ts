export const getSelectedLanguage = () => {
  return window.localStorage.getItem("i18nextLng") || "ru";
};
