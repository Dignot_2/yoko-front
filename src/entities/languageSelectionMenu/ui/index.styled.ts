import styled from "@emotion/styled";

export const LngInformationBlock = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  gap: 12px;
  width: 100%;
`;

export const LngText = styled.p`
  font-style: normal;
  font-weight: 500;
  font-size: 13px;
  line-height: 14px;
  color: #181938;
`;
