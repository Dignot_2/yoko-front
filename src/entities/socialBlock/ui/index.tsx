import { IconButton } from "@mui/material";
import TelegramIcon from "./icons/telegram.svg";
import InstagramIcon from "./icons/instagram.svg";
import VKIcon from "./icons/vk.svg";
import TwitterIcon from "./icons/twitter.svg";
import * as Styled from "./index.styled";

const SocialBlock = () => {
  const handleNavigateTelegram = () => {
    window.open("https://t.me/yokotrade", "mozillaTab");
  };

  const handleNavigateInstagram = () => {
    window.open("https://instagram.com/yokotrade?igshid=MDM4ZDc5MmU=", "mozillaTab");
  };

  return (
    <Styled.SocialBlock>
      <IconButton>
        <TwitterIcon />
      </IconButton>
      <IconButton onClick={() => handleNavigateTelegram()}>
        <TelegramIcon />
      </IconButton>
      <IconButton>
        <VKIcon />
      </IconButton>
      <IconButton onClick={() => handleNavigateInstagram()}>
        <InstagramIcon />
      </IconButton>
    </Styled.SocialBlock>
  );
};

export default SocialBlock;
