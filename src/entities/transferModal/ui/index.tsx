import { ChangeEvent, useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import Button from "shared/ui/Button";
import ModalPage from "shared/ui/ModalPage";
import Loading from "shared/ui/Loading";
import type { ITransferModalProps, ITransferModalFormValue } from "./index.types";
import * as Styled from "./index.styled";

const TransferModal = (props: ITransferModalProps) => {
  const { t } = useTranslation();

  const { isOpen, from, to, title, action, handleCLose, isLoading, type, mainError } = props;
  const { title: buttonTitle, action: handleClick } = action;

  const [error, setError] = useState<string | null>(null);
  const [toInputColor, setToInputColor] = useState("#181938");
  const [formValues, setFormValues] = useState<ITransferModalFormValue>({
    fromValue: from?.initValue ? `${from?.initValue}` : "",
    toValue: `${to?.initValue}`,
  });

  useEffect(() => {
    setError(mainError);
  }, [mainError]);

  const handleChange = (value: string) => {
    const { max, min } = from;
    const { initValue } = to;

    if (type === "add") {
      setFormValues(() => ({
        fromValue: value,
        toValue: `${(initValue ? +initValue : 0) + +value}`,
      }));
    }

    if (type === "remove") {
      setFormValues(() => ({
        fromValue: value,
        toValue: `${(initValue ? +initValue : 0) - +value}`,
      }));
    }

    if (max && max < +value) {
      setError("transfer.modal.error.max");
      return;
    }
    if (min && min > +value) {
      setError("transfer.modal.error.min");
      return;
    }
    setError(null);
  };

  const handleSubmit = () => {
    if (!error) {
      const [deposit] = formValues.fromValue.split(" ");
      handleClick(+deposit);
    }
  };

  const getOnBlurValue = () => {
    setFormValues((prev) => {
      const { fromValue } = prev;

      if (!fromValue) {
        return {
          ...prev,
          fromValue: "",
        };
      }
      if (fromValue) {
        return {
          ...prev,
          fromValue: `${fromValue} USDT`,
        };
      }
    });
  };

  const getOnFocusValue = () => {
    setFormValues((prev) => {
      const { fromValue } = prev;
      const [price] = fromValue.split(" ");

      return {
        ...prev,
        fromValue: price,
      };
    });
  };

  const getFromValue = (): string => {
    const { fromValue } = formValues;
    if (!fromValue) return "";
    return fromValue;
  };

  useEffect(() => {
    const { toValue } = formValues;

    const [valueTo] = toValue.split(" ");
    if (+valueTo === 0) {
      setToInputColor("#f87c92");
    } else {
      setToInputColor("#05cd99");
    }
  }, [formValues]);

  if (!isOpen) return null;

  return (
    <ModalPage onClose={() => handleCLose()}>
      <Styled.TransferModalWrapper>
        {isLoading && <Loading />}
        {!isLoading && (
          <>
            <Styled.TransferModalTitle>{t(title)}</Styled.TransferModalTitle>
            <Styled.TransferModalInputWrapper>
              <Styled.TransferModalInputBlock>
                <Styled.TransferModalInputlabel htmlFor="to.input">
                  {t(to.label)}
                </Styled.TransferModalInputlabel>
                <Styled.TransferModalInput
                  id="to.input"
                  color={toInputColor}
                  placeholder={t(to.placeholder)}
                  value={
                    Number.isInteger(Number(formValues["toValue"]))
                      ? formValues["toValue"] + " USDT"
                      : Number(formValues["toValue"]).toFixed(6) + " USDT"
                  }
                  disabled
                />
              </Styled.TransferModalInputBlock>
              <Styled.TransferModalInputBlock>
                <Styled.TransferModalInputlabel htmlFor="from.input">
                  {t(from.label)}
                </Styled.TransferModalInputlabel>
                <Styled.TransferModalInput
                  id="from.input"
                  color="#181938"
                  placeholder={t(from.placeholder)}
                  defaultValue={from?.initValue && `${from?.initValue} USDT`}
                  value={getFromValue()}
                  onBlur={() => getOnBlurValue()}
                  onFocus={() => getOnFocusValue()}
                  onChange={(evt: ChangeEvent<HTMLInputElement>) => handleChange(evt.target.value)}
                />
              </Styled.TransferModalInputBlock>
            </Styled.TransferModalInputWrapper>
            <Styled.TransferModalInformationBlock>
              {<Styled.TransferModalError>{t(error)}</Styled.TransferModalError>}
              <Button size="medium" onClick={() => handleSubmit()} color="primary">
                {t(buttonTitle)}
              </Button>
            </Styled.TransferModalInformationBlock>
          </>
        )}
      </Styled.TransferModalWrapper>
    </ModalPage>
  );
};

export default TransferModal;
export { ITransferModalProps };
