import styled from "@emotion/styled";

export const TransferModalWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-flow: column;
  align-items: center;
  justify-content: flex-start;
  padding: 0 100px;
  gap: 56px;

  @media screen and (max-width: 670px) {
    padding: 0 16px;
    gap: 40px;
  }
`;

export const TransferModalTitle = styled.div`
  font-style: normal;
  font-weight: 600;
  font-size: 40px;
  line-height: 24px;
  text-align: center;
  color: #181938;

  @media screen and (max-width: 670px) {
    font-size: 30px;
    line-height: 32px;
  }
`;

export const TransferModalInputWrapper = styled.div`
  display: flex;
  flex-flow: column;
  align-items: center;
  justify-content: flex-start;
  gap: 24px;
`;

export const TransferModalInputBlock = styled.div`
  padding: 16px 0;
  display: flex;
  flex-flow: column;
  align-items: center;
  justify-content: center;
  gap: 8px;

  width: 371px;
  height: 76px;
  background: #ffffff;
  box-shadow: 0px 25px 20px -25px rgba(20, 25, 143, 0.05);
  border-radius: 20px;

  @media screen and (max-width: 670px) {
    width: 235px;
  }
`;

export const TransferModalInputlabel = styled.label`
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 12px;
  letter-spacing: 0.3px;
  color: #181938;
`;

type IInputValueForColor = {
  color: string;
};

export const TransferModalInput = styled.input<IInputValueForColor>`
  font-style: normal;
  font-weight: 900;
  font-size: 24px;
  line-height: 24px;
  text-align: center;
  letter-spacing: 0.3px;
  color: ${({ color }) => color} !important;
  border: none;

  &:placeholder-shown {
    font-style: normal;
    font-weight: 900;
    font-size: 24px;
    line-height: 24px;
    letter-spacing: 0.3px;
    color: #959db5;
  }

  &:disabled {
    background: none !important;
  }

  &:focus-visible {
    border: none !important;
    outline: none !important;
  }
`;

export const TransferModalInformationBlock = styled.div`
  margin-top: 10px;
  display: flex;
  flex-flow: column;
  align-items: center;
  justify-content: flex-start;
  width: 100%;
  gap: 40px;
`;

export const TransferModalError = styled.p`
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 14px;
  color: #f87c92;
  min-height: 20px;
`;
