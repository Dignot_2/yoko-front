import { createSlice } from "@reduxjs/toolkit";
import _ from "lodash";
import { getBotSettingsAction } from "widgets/botSettingsTable";
import { getBotInformationAction } from "widgets/botInformationCard";
import { changeBotStatusActions } from "features/botOn";
import { changeBotKeysAction } from "features/changeBotKeys";
import { changeBotSettingsAction } from "features/changeBotSettings/model";
import { BotStatus } from "shared/constants/botStatus";
import type { InitialState } from "./index.types";

const initialState: InitialState = {
  loading: false,
  error: null,
  botInformation: null,
  keys: null,
  changeBotKeysLoading: false,
  settings: null,
  changeBotSettingsLoading: false,
  changeBotStatusLoading: false,
};

const botSlice = createSlice({
  name: "bot",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getBotInformationAction.fulfilled, (state, { payload }) => {
      state.botInformation = payload;
      state.loading = false;
    });
    builder.addCase(getBotInformationAction.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getBotInformationAction.rejected, (state, { payload }) => {
      state.loading = false;
      state.error = payload;
    });
    builder.addCase(changeBotStatusActions.fulfilled, (state, { payload }) => {
      state.botInformation.status = payload.status as BotStatus;
      state.loading = false;
    });
    builder.addCase(changeBotStatusActions.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(changeBotStatusActions.rejected, (state, { payload }) => {
      state.loading = false;
      state.error = payload;
    });
    builder.addCase(changeBotKeysAction.fulfilled, (state, { payload }) => {
      state.keys = payload;
      state.changeBotKeysLoading = false;
    });
    builder.addCase(changeBotKeysAction.pending, (state) => {
      state.changeBotKeysLoading = true;
    });
    builder.addCase(changeBotKeysAction.rejected, (state, { payload }) => {
      state.changeBotKeysLoading = false;
      state.error = payload;
    });
    builder.addCase(getBotSettingsAction.fulfilled, (state, { payload }) => {
      const keys = _.pick(payload, ["apiKey", "secretKey"]);
      const settings = _.omit(payload, ["apiKey", "secretKey"]);
      state.keys = keys;
      state.settings = settings;
      state.loading = false;
    });
    builder.addCase(getBotSettingsAction.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getBotSettingsAction.rejected, (state, { payload }) => {
      state.loading = false;
      state.error = payload;
    });
    builder.addCase(changeBotSettingsAction.fulfilled, (state, { payload }) => {
      state.settings = payload;
      state.changeBotSettingsLoading = false;
    });
    builder.addCase(changeBotSettingsAction.pending, (state) => {
      state.changeBotSettingsLoading = true;
    });
    builder.addCase(changeBotSettingsAction.rejected, (state, { payload }) => {
      state.changeBotSettingsLoading = false;
      state.error = payload;
    });
  },
});

export default botSlice.reducer;
