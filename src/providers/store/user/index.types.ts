import { IUser } from "shared/api/user/getUser";

export interface InitialState {
  user?: IUser | null;
  loading: boolean;
  error?: string | null;
  confirmationLoading: boolean;
}
