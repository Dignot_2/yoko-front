import { useDispatch } from "react-redux";
import type { AppDispatch } from "src/providers/store";

export const useAppDispatch = () => useDispatch<AppDispatch>();
