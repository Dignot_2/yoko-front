import { configureStore, combineReducers } from "@reduxjs/toolkit";
import type { IStore } from "./store.types";
import userReduser from "./user";
import botReducer from "./bot";
import referralReducer from "./referral";

export const reducer = combineReducers<IStore>({
  userReduser,
  botReducer,
  referralReducer,
});

export const store = configureStore({
  reducer,
});
