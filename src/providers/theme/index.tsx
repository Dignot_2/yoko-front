import { createTheme } from "@mui/material/styles";

declare module "@mui/material/styles" {
  interface Palette {
    colorGrey: string;
    colorWhite: string;
    colorText: string;
    colorTextRed: string;
    colorTextGreen: string;
    colorAccent: string;
    colorWindow: string;
    colorShade70: string;
    colorText2: string;
    colorMenuColor: string;
    colorStatus3: string;
  }

  interface PaletteOptions {
    colorGrey: string;
    colorWhite: string;
    colorText: string;
    colorTextRed: string;
    colorTextGreen: string;
    colorAccent: string;
    colorWindow: string;
    colorShade70: string;
    colorText2: string;
    colorMenuColor: string;
    colorStatus3: string;
  }

  interface TypographyVariants {
    body: React.CSSProperties;
    mobileBody: React.CSSProperties;
    menuDesktop: React.CSSProperties;
    buttonMobile: React.CSSProperties;
  }

  // allow configuration using `createTheme`
  interface TypographyVariantsOptions {
    body?: React.CSSProperties;
    mobileBody?: React.CSSProperties;
    menuDesktop?: React.CSSProperties;
    buttonMobile?: React.CSSProperties;
  }
}

// Update the Typography's variant prop options
declare module "@mui/material/Typography" {
  interface TypographyPropsVariantOverrides {
    body: true;
    mobileBody: true;
    menuDesktop: true;
    buttonMobile: true;
    h3: false;
    h1: false;
    h2: false;
    h5: false;
    h4: false;
  }
}

const theme = createTheme({
  palette: {
    primary: {
      main: "#5F5CEC",
    },
    colorGrey: "#F5F7FD",
    colorWhite: "#fff",
    colorText: "#181938",
    colorTextRed: "#D62E35",
    colorTextGreen: "#05CD99",
    colorAccent: "#5F5CEC",
    colorWindow: "#9298B8",
    colorShade70: "#2E3A59",
    colorText2: "#959DB5",
    colorMenuColor: "#CFCEF9",
    colorStatus3: "#F4CA36",
  },
  typography: {
    h1: {
      fontSize: "48px",
      lineHeight: "56px",
      fontWeight: 700,
    },
    h2: {
      fontSize: "40px",
      lineHeight: "48px",
      fontWeight: 600,
    },
    h3: {
      fontSize: "30px",
      lineHeight: "32px",
      fontWeight: 600,
    },
    h4: {
      fontSize: "20px",
      lineHeight: "24px",
      fontWeight: 600,
    },
    body: {
      fontSize: "24px",
      lineHeight: "32px",
      fontWeight: 400,
    },
    mobileBody: {
      fontSize: "14px",
      lineHeight: "14px",
      fontWeight: 500,
    },
    menuDesktop: {
      fontSize: "16px",
      lineHeight: "24px",
      fontWeight: 400,
    },
    button: {
      fontSize: "16px",
      lineHeight: "16px",
      fontWeight: 600,
    },
    buttonMobile: {
      fontSize: "14px",
      lineHeight: "14px",
      fontWeight: 600,
    },
  },
});

export default theme;
