export const getFormatDate = (date: string) =>
  new Date(date).toLocaleString("ru-GB", { timeZone: "UTC" });
