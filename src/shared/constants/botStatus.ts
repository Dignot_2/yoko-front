export enum BotStatus {
  CREATE = "CREATE",
  ON = "ON",
  OFF = "OFF",
  ERROR = "ERROR",
}
