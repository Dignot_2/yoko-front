import styled from "@emotion/styled";

export const SuccessStatus = styled.div`
  padding: 3px 8px;
  display: flex;
  align-items: center;
  justify-content: center;
  width: fit-content;
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 16px;
  letter-spacing: 0.2px;
  color: #000000;
  height: 24px;
  background: #e4fde9;
  border-radius: 4px;
`;

export const PendingStatus = styled.div`
  padding: 3px 8px;
  display: flex;
  align-items: center;
  justify-content: center;
  width: fit-content;
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 16px;
  letter-spacing: 0.2px;
  color: #000000;
  height: 24px;
  background: #e9e9e9;
  border-radius: 4px;
`;

export const ErrorStatus = styled.div`
  padding: 3px 8px;
  display: flex;
  align-items: center;
  justify-content: center;
  width: fit-content;
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 16px;
  letter-spacing: 0.2px;
  color: #000000;
  height: 24px;
  background: #e8ebff;
  border-radius: 4px;
`;

export const ProcessedStatus = styled.div`
  padding: 3px 8px;
  display: flex;
  align-items: center;
  justify-content: center;
  width: fit-content;
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 16px;
  letter-spacing: 0.2px;
  color: #000000;
  height: 24px;
  background: #e8ebff;
  border-radius: 4px;
`;

export const ExpiredStatus = styled.div`
  padding: 3px 8px;
  display: flex;
  align-items: center;
  justify-content: center;
  width: fit-content;
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 16px;
  letter-spacing: 0.2px;
  color: #000000;
  height: 24px;
  background: #ffe8e8;
  border-radius: 4px;
`;
