import LinkBadge from "../LinkBadge/LinkBadge";
import StatusBadge from "../StatusBadge/StatusBadge";
import TextBadge from "../TextBadge/TextBadge";
import { IRowCell } from "../../Table.types";
import { IStatusBadgeProps } from "../StatusBadge/StatusBadge.types";

const CellWrapper = (props: IRowCell) => {
  const { type, content, statusType } = props;
  switch (type) {
    case "link":
      return <LinkBadge {...props} />;
    case "status":
      const status = statusType as IStatusBadgeProps["status"];
      return <StatusBadge {...{ content, status }} />;
    default:
      return <TextBadge {...props} />;
  }
};

export default CellWrapper;
