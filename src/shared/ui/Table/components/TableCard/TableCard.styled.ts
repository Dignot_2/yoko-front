import styled from "@emotion/styled";

type IIsHover = {
  isHover: boolean;
};

export const TableCardWrapper = styled.div<IIsHover>`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  padding: 16px;
  gap: 16px;
  width: 100%;
  height: fit-content;
  background: #ffffff;
  box-shadow: 0px 25px 20px -25px rgba(20, 25, 143, 0.15);
  border-radius: 10px;
  border: 1px solid #dfe5f8;

  &:hover {
    ${({ isHover }) =>
      isHover &&
      `
    cursor: pointer;
    border: 1px solid #5F5CEC;
    box-shadow: 0px 25px 20px -25px rgba(20, 25, 143, 0.15);
  `}
  }
`;

type IIsNotBorder = {
  isNotBorder?: boolean;
};

export const TableCardMainWrapper = styled.div<IIsNotBorder>`
  padding-bottom: ${({ isNotBorder }) => (isNotBorder ? "0" : "16px")};
  display: flex;
  flex-flow: row nowrap;
  align-items: center;
  justify-content: space-between;
  border-bottom: ${({ isNotBorder }) => (isNotBorder ? "none" : "1px solid #dfe5f8")};
  width: 100%;
`;

export const TableCardMainInfomation = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: space-between;
  gap: 4px;
`;

export const TableCardMainInfomationTitle = styled.p`
  font-style: normal;
  font-weight: 600;
  font-size: 16px;
  line-height: 16px;
  color: #181938;
`;

export const TableCardMainInfomationDescription = styled.p`
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 16px;
  text-align: left;
  letter-spacing: 0.2px;
  color: #9298b8;
  max-width: 145px;
  text-overflow: ellipsis;
`;

export const DescriptionBlockWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const TableCell = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: center;
  gap: 4px;
`;

export const TableCellTitle = styled.h2`
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 16px;
  letter-spacing: 0.2px;
  color: #9298b8;
`;

export const TableCellValue = styled.p`
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 16px;
  letter-spacing: 0.2px;
  color: #181938;
`;
