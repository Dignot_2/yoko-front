import { IRow } from "../../Table.types";

export type ITableCardProps = Pick<IRow, "mobileCard"> & {
  handleSelectCard: () => void;
  isHover: boolean;
};
