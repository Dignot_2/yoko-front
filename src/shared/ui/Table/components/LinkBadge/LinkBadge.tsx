import { useTranslation } from "react-i18next";
import { IRowCell } from "../../Table.types";
import * as Styled from "./LinkBadge.styled";

const LinkBadge = (props: Pick<IRowCell, "content" | "textLink" | "position">) => {
  const { textLink, content } = props;
  const { t } = useTranslation();

  return (
    <Styled.LinkBadgeWrapper href={content} target="_blank" {...props}>
      {t(textLink)}
    </Styled.LinkBadgeWrapper>
  );
};

export default LinkBadge;
