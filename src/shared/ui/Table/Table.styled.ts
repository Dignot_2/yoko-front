import { bool } from "yup";
import { IHeadCell, IRowCell } from "./Table.types";
import styled from "@emotion/styled";

type IRowFlexPosition = {
  rowFlexPosition?: string;
};

export const HomePageEmptyMessageBlock = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const HomePageEmptyMessage = styled.h2`
  font-style: normal;
  font-weight: 600;
  font-size: 24px;
  line-height: 26px;
  letter-spacing: 0.5px;
  color: #9298b8;
  text-align: center;

  @media screen and (max-width: 800px) {
    flex-flow: column nowrap;
    font-size: 12px;
    line-height: 17px;
  }
`;

type IIsMobile = {
  isMobile: boolean;
};

type IIsScrollHidden = {
  isScrollHidden: boolean;
};

export const TableContentWrapper = styled.div<IIsMobile>`
  display: flex;
  flex-flow: column nowrap;
  align-items: center;
  justify-content: flex-start;
  width: 100%;
  height: 100%;
  gap: ${({ isMobile }) => (isMobile ? "22px" : "0")};
`;

export const TableContentTitle = styled.h1<IIsMobile>`
  margin-bottom: 24px;
  width: 100%;
  text-align: left;
  font-style: normal;
  font-weight: 600;
  font-size: ${({ isMobile }) => (!isMobile ? "20px" : "14px")};
  line-height: 24px;
  letter-spacing: 0.3px;
  color: #181938;
  & > span {
    color: #5f5cec;
  }
`;

type ITableWrapperProps = IIsMobile & IIsScrollHidden;

export const TableWrapper = styled.table<ITableWrapperProps>`
  display: flex;
  flex-flow: column nowrap;
  align-items: center;
  justify-content: flex-start;
  width: 100%;
  height: fit-content;
  overflow-y: ${({ isScrollHidden }) => (isScrollHidden ? "hidden" : "auto")};
  gap: ${({ isMobile }) => (isMobile ? "16px" : "0")};
`;

export const TableHeader = styled.thead<IRowFlexPosition>`
  padding: 0 16px;
  width: 100%;
  height: 48px;
  background: #f5f7fd;
  border-bottom: 1px solid #dfe5f8;
  display: flex;
  flex-flow: row nowrap;
  align-items: flex-start;
  justify-content: ${({ rowFlexPosition }) => (rowFlexPosition ? rowFlexPosition : "flex-start")};
`;

export const TableHeadCell = styled.td<Pick<IHeadCell, "position" | "width" | "flexPosition">>`
  width: ${({ width }) => width};
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: ${({ flexPosition }) => flexPosition};

  & > p {
    font-style: normal;
    font-weight: 600;
    font-size: 14px;
    line-height: 16px;
    display: flex;
    align-items: center;
    letter-spacing: 0.3px;
    color: #9298b8;
    text-align: ${({ position }) => position};
  }
`;

export const TableBody = styled.tbody`
  width: 100%;
  display: flex;
  flex-flow: column nowrap;
  align-items: flex-start;
  justify-content: space-between;
`;

type IIsHover = {
  isHover: boolean;
};

export const TableRow = styled.tr<IIsHover & IRowFlexPosition>`
  padding: 0 16px;
  width: 100%;
  height: 56px;
  background: #ffffff;
  border-bottom: 1px solid #dfe5f8;
  display: flex;
  flex-flow: row nowrap;
  align-items: flex-start;
  justify-content: ${({ rowFlexPosition }) => (rowFlexPosition ? rowFlexPosition : "flex-start")};

  &:hover {
    ${({ isHover }) =>
      isHover &&
      `
    cursor: pointer;
    background-color: #f5f7fd;
  `}
  }
`;

export const TableRowCell = styled.td<Pick<IRowCell, "position" | "width" | "flexPosition">>`
  width: ${({ width }) => width};
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: ${({ flexPosition }) => flexPosition};

  & > p {
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-box-orient: vertical;
    -webkit-line-clamp: 2;
    overflow: hidden;
    word-wrap: break-word;
  }
`;

export const TablePaganationBlock = styled.div<IIsMobile>`
  padding-top: ${({ isMobile }) => (isMobile ? "16px" : "0")};
  width: 100%;
  height: 55px;
  display: flex;
  align-items: center;
  justify-content: center;
  border-top: ${({ isMobile }) => (isMobile ? "none" : "1px solid rgba(0, 0, 0, 0.1)")};
`;
