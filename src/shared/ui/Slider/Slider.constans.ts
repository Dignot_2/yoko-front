import { Navigation, Pagination } from "swiper";
import { SliderSettings } from "./Slider.types";

export const settingSlider: SliderSettings = {
  modules: [Navigation, Pagination],
  navigation: {
    nextEl: ".arrow-right",
    prevEl: ".arrow-left",
  },
  pagination: {
    el: ".paginationWrapper",
    clickable: true,
  },
  grabCursor: true,
  slidesPerView: 1.35,
  spaceBetween: 10,
  loop: true,
  breakpoints: {
    640: {
      slidesPerView: 2,
      spaceBetween: 20,
    },
    768: {
      slidesPerView: 2,
      spaceBetween: 24,
    },
    1024: {
      slidesPerView: 3,
      spaceBetween: 24,
    },
    1280: {
      slidesPerView: 5,
      spaceBetween: 25,
    },
  },
};
