import { useState, MouseEvent } from "react";
import { useTranslation } from "react-i18next";
import InfoIcon from "@mui/icons-material/Info";
import Tooltip from "@mui/material/Tooltip";
import IconButton from "@mui/material/IconButton";
import Menu from "@mui/material/Menu";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import Loading from "../Loading";
import type { IActionsCard } from "./actionsCard.types";
import * as Styled from "./actionsCard.styled";

const ActionsCard = (props: IActionsCard) => {
  const { t } = useTranslation();

  const {
    Icon,
    title,
    description,
    info,
    actions,
    descriptionColor,
    Action,
    isLoading,
    errorInfo,
  } = props;

  const [openMenu, setOpenMenu] = useState(false);
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);

  const handleOpenMenu = (event: MouseEvent<HTMLElement>) => {
    if (event.currentTarget) {
      setAnchorEl(event.currentTarget);
    }
    setOpenMenu((prev) => !prev);
  };

  return (
    <Styled.ActionsCardWrapper>
      {isLoading && <Loading />}
      <Styled.ActionsCardMainBlock>
        <Icon />
        <Styled.ActionsCardInformationBlock>
          <Styled.ActionsCardTitle>{t(title)}</Styled.ActionsCardTitle>
          {description && (
            <Styled.ActionsCardDescriptionBlock>
              <Styled.ActionsCardDescription
                color={descriptionColor ? descriptionColor : "#05CD99"}
              >
                {t(description)}
              </Styled.ActionsCardDescription>
            </Styled.ActionsCardDescriptionBlock>
          )}
        </Styled.ActionsCardInformationBlock>
      </Styled.ActionsCardMainBlock>
      <Styled.ActionsCardActionsBLock>
        {errorInfo && (
          <Tooltip title={t(errorInfo)}>
            <InfoIcon color="error" />
          </Tooltip>
        )}
        {info && (
          <Tooltip title={t(info)}>
            <InfoIcon color="primary" />
          </Tooltip>
        )}
        {Action && <Action />}
        {actions?.length && (
          <Styled.ActionsCardPopupMenuWrapper>
            <IconButton
              aria-label="more"
              id="long-button"
              aria-controls={openMenu ? "long-menu" : undefined}
              aria-expanded={openMenu ? "true" : undefined}
              aria-haspopup="true"
              onClick={handleOpenMenu}
              href=""
            >
              <MoreVertIcon />
            </IconButton>
            <Menu
              id="long-menu"
              MenuListProps={{
                "aria-labelledby": "long-button",
              }}
              anchorEl={anchorEl}
              open={openMenu}
              onClose={handleOpenMenu}
              PaperProps={{
                style: {
                  maxHeight: 48 * 4.5,
                  width: "20ch",
                },
              }}
            >
              {actions?.map((Action) => (
                <Action />
              ))}
            </Menu>
          </Styled.ActionsCardPopupMenuWrapper>
        )}
      </Styled.ActionsCardActionsBLock>
    </Styled.ActionsCardWrapper>
  );
};

export default ActionsCard;
