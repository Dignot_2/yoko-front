import styled from "@emotion/styled";

export const ModalPageWrapper = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  z-index: 1111;

  &::before {
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background: rgba(24, 25, 56, 0.25);
  }
`;

export const ModalPageContent = styled.div`
  position: relative;
  padding: 120px 0px 60px;
  width: 800px;
  height: 788px;
  background: #ffffff;
  border-radius: 20px;
  display: flex;
  align-items: center;
  justify-content: center;

  @media screen and (max-width: 800px) {
    padding: 64px 16px 36px;
    width: 342px;
    height: 684px;
  }
`;

export const ModalPageClosedButton = styled.div`
  position: absolute;
  top: 25px;
  right: 25px;
  cursor: pointer;
  z-index: 100;
  background: #d9d9d9;
  width: 64px;
  height: 64px;
  display: flex;
  align-items: center;
  justify-content: center;
  box-sizing: border-box;
  border-radius: 50%;

  & > svg {
    width: 26.67px;
    height: 26.67px;
  }

  @media screen and (max-width: 800px) {
    width: 36px;
    height: 36px;

    & > svg {
      width: 15px;
      height: 15px;
    }
  }
`;
