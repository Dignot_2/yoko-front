import styled from "@emotion/styled";

export const ErrorPageWrapper = styled.div`
  margin-top: 10%;
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-flow: column nowrap;
  gap: 5%;
`;

export const ErrorImage = styled.img`
  width: 25%;
  height: 25%;
`;

export const ErrorMessage = styled.h2`
  margin: 5% 0 5% 0;
  font-family: "Roboto";
  font-style: normal;
  font-weight: 900;
  font-size: 34px;
  line-height: 34px;
  letter-spacing: 0.3px;
  color: #181938;
`;
