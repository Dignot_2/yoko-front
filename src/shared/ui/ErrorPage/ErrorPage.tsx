// import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
// import Button from "ui/Button";
import RobotImagPath from "assets/main-lending/robot.png";
import * as Styled from "./ErrorPage.styled";

const ErrorPage = () => {
  // const { t } = useTranslation();
  const navigate = useNavigate();

  const handleNavigate = () => {
    navigate("");
  };
  return (
    <Styled.ErrorPageWrapper>
      Error
      {/* // TODO - create this page */}
      {/* <Styled.ErrorImage src={RobotImagPath} alt="robot image" /> */}
      {/* <Styled.ErrorMessage>{t("pages.error_page_message")}</Styled.ErrorMessage> */}
      {/* <Button
        title={t("pages.general")}
        onClick={() => handleNavigate()}
        background="#5F5CEC"
        textColor="#FFFFFF"
        borderRadius="10px"
        width="10%"
      /> */}
    </Styled.ErrorPageWrapper>
  );
};

export default ErrorPage;
