import ErrorIconPath from "shared/image/errorIcon.png";
import * as Styled from "./index.styled";

const SuccessIconBlock = () => {
  return (
    <Styled.ErrorIconBlockWrapper>
      <Styled.ErrorIcon src={ErrorIconPath} alt="icon for succses" />
    </Styled.ErrorIconBlockWrapper>
  );
};

export default SuccessIconBlock;
