import React from "react";
import { ButtonProps } from "@mui/material/Button";
import Loading from "../Loading/Loading";
import * as Styled from "./Button.styled";

type IButtonProps = {
  children: string | React.ReactNode;
  size: ButtonProps["size"];
  leftIcon?: React.ReactNode;
  rigthIcon?: React.ReactElement;
  variant?: ButtonProps["variant"];
  color?: ButtonProps["color"];
  onClick?: () => void;
  isLoading?: boolean;
};

const Button = (props: IButtonProps) => {
  const { children, variant, size, color, onClick, leftIcon, isLoading } = props;
  return (
    <Styled.ButtonWrapper {...{ variant, size, color, onClick, leftIcon }}>
      {isLoading && <Loading />}
      {!isLoading && children}
    </Styled.ButtonWrapper>
  );
};

export default Button;
