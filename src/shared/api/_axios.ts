import axios from "axios";
import { getResponseData } from "../api/lib/getResponseData";
import { setRequestHeaders } from "../api/lib/setRequestHeaders";
export class Http {
  _instance;
  constructor(private readonly apiUrl = "/") {
    this._instance = axios.create({
      baseURL: this.apiUrl,
      withCredentials: true,
    });

    this._instance.interceptors.request.use(
      function (config) {
        // Do something before request is sent
        const headers = setRequestHeaders();
        config.headers = { ...config.headers, ...headers };
        return config;
      },
      function (error) {
        // Do something with request error
        return Promise.reject(error);
      }
    );

    this._instance.interceptors.response.use(
      function (response) {
        // Any status code that lie within the range of 2xx cause this function to trigger
        // Do something with response data
        return getResponseData(response);
      },
      function (error) {
        // Any status codes that falls outside the range of 2xx cause this function to trigger
        // Do something with response error
        return Promise.reject(error?.response?.data?.message);
      }
    );
  }

  async post<T = unknown, K = unknown>(url: string, body: K): Promise<T> {
    try {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      return await this._instance.post<K, T>(url, body);
    } catch (e) {
      throw e;
    }
  }

  async patch<T = unknown, K = unknown>(url: string, body: K): Promise<T> {
    try {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      return await this._instance.patch<K, T>(url, body);
    } catch (e) {
      throw e;
    }
  }

  async get<T = unknown>(url: string): Promise<T> {
    try {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      return await this._instance.get<any, T>(url);
    } catch (e) {
      throw e;
    }
  }
}
