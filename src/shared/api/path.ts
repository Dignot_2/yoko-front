export const DOMAIN = `${window.location.protocol}//${window.location.hostname}`;
// Server
// export const API_PATH = `${window.location.protocol}//api.${window.location.hostname}/api`;
// local
export const API_PATH = "https://api.test.yokotrade.io/api/";

export const GET_USER_DEPOSIT_PATH = "/user/deposit";
