import { AxiosResponse } from "axios";
import { LOCAL_STORAGE_TOKEN_NAME } from "shared/constants/variable";

export const getResponseData = (response: AxiosResponse) => {
  const responseData = response.data;
  const token = responseData?.token;
  if (token) {
    window.localStorage.setItem(LOCAL_STORAGE_TOKEN_NAME, token);
  }
  return responseData;
};
