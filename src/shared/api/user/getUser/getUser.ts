import http from "shared/api/axios";
import type { IUser } from "./user.types";

export const getUserApi = () => http.get<IUser>("user");
