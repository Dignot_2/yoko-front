import { Http } from "./_axios";
import { API_PATH } from "./path";
const http = new Http(API_PATH);

export default http;
